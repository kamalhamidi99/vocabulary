﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SQLite;
using Vocabulary.Entity;

namespace Vocabulary.Services
{
    public class SettingService : IDataStore<SettingEntity>
    {
        readonly SQLiteAsyncConnection _database;

        public SettingService()
        {
            _database = App.SqLiteAsyncConnection;
            _database.CreateTableAsync<SettingEntity>().Wait();
        }

        public async Task<bool> SaveAsync(SettingEntity item)
        {
            if (item.Id != 0)
                return await UpdateAsync(item);

            return await AddAsync(item);
        }

        public async Task<bool> AddAsync(SettingEntity item)
        {
            var r = await _database.InsertAsync(item);
            return r > 0;
        }

        public async Task<bool> UpdateAsync(SettingEntity item)
        {
            var r = await _database.UpdateAsync(item);
            return r > 0;
        }

        public Task<bool> DeleteAsync(int id)
        {
            throw new System.NotImplementedException();
        }


        public async Task<SettingEntity> GetAsync(int id = 1)
        {
            var model = await _database.Table<SettingEntity>().Where(i => i.Id == id).FirstOrDefaultAsync();
            if (model == null)
            {
                model = new SettingEntity();
                await AddAsync(model);
            }
            return model;
        }

        public Task<List<SettingEntity>> GetAllAsync()
        {
            throw new System.NotImplementedException();
        }
    }
}