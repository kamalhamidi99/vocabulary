﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using SQLite;
using Vocabulary.Entity;
using Xamarin.Forms;

namespace Vocabulary.Services
{
    public class BookService : IDataStore<BookEntity>
    {
        readonly SQLiteAsyncConnection _database;

        public BookService()
        {
            _database = App.SqLiteAsyncConnection;
            _database.CreateTableAsync<BookEntity>().Wait();
        }

        public async Task<bool> SaveAsync(BookEntity item)
        {
            if (item.Id != 0)
                return await UpdateAsync(item);

            return await AddAsync(item);
        }

        public async Task<bool> AddAsync(BookEntity item)
        {
            var r = await _database.InsertAsync(item);
            return r > 0;
        }

        public async Task<bool> UpdateAsync(BookEntity item)
        {
            var r = await _database.UpdateAsync(item);
            return r > 0;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var book = await  _database.Table<BookEntity>().Where(i => i.Id == id).FirstOrDefaultAsync();
            var lessons = await App.LessonService.GetAllAsync(book.Id);
            foreach (var lessonEntity in lessons)
            {
                var words = await App.WordService.SearchAsync(lessonId: lessonEntity.Id);
                foreach (var wordEntity in words)
                {
                    await App.WordService.DeleteAsync(wordEntity.Id);
                }
                await App.LessonService.DeleteAsync(lessonEntity.Id);
            }
            var r = await _database.DeleteAsync(book);
            return r > 0;
        }

        public async Task<BookEntity> GetAsync(int id)
        {
            var get = await _database.Table<BookEntity>().Where(i => i.Id == id).FirstOrDefaultAsync();
            get.CoverImage = ImageSource.FromStream(
                () => new MemoryStream(Convert.FromBase64String(get.Cover)));
            return get;
        }
        public async Task<BookEntity> GetAsync(string code)
        {
            return await _database.Table<BookEntity>().Where(i => i.Code.ToLower() == code.ToLower()).FirstOrDefaultAsync();
        }

        public async Task<List<BookEntity>> GetAllAsync()
        {
            return await _database.Table<BookEntity>().ToListAsync();
        }
    }
}