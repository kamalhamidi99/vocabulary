﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SQLite;
using Vocabulary.Entity;

namespace Vocabulary.Services
{
    public class WordService : IDataStore<WordEntity>
    {
        readonly SQLiteAsyncConnection _database;

        public WordService()
        {
            _database = App.SqLiteAsyncConnection;
            _database.CreateTableAsync<WordEntity>().Wait();
        }

        public async Task<bool> SaveAsync(WordEntity item)
        {
            if (item.Id != 0)
                return await UpdateAsync(item);

            return await AddAsync(item);
        }

        public async Task<bool> AddAsync(WordEntity item)
        {
            var r = await _database.InsertAsync(item);
            return r > 0;
        }

        public async Task<bool> UpdateAsync(WordEntity item)
        {
            var r = await _database.UpdateAsync(item);
            return r > 0;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var item = await _database.Table<WordEntity>().Where(i => i.Id == id).FirstOrDefaultAsync();
            var r = await _database.DeleteAsync(item);
            return r > 0;
        }

        public async Task<WordEntity> GetAsync(int id)
        {
            return await _database.Table<WordEntity>().Where(i => i.Id == id).FirstOrDefaultAsync();
        }

        public async Task<List<WordEntity>> GetAllAsync()
        {
            return await _database.Table<WordEntity>().ToListAsync();
        }

        public async Task<List<WordEntity>> SearchAsync(int? page = null, int? lessonId = null, int? bookId = null, string word = null)
        {
            var e =  _database.Table<WordEntity>();

            if (lessonId != null)
            {
                e = e.Where(x => x.LessonId == lessonId);
            }

            if (bookId != null)
            {
                e = e.Where(x => x.BookId == bookId);
            }

            if (!string.IsNullOrWhiteSpace(word))
            {
                e = e.Where(x => x.Title.ToLower().Contains(word.ToLower()));
            }

            if (page != null)
            {
                e = e.Skip((page.Value - 1) * 20).Take(20);
            }

            return await e.ToListAsync();
        }
    }
}