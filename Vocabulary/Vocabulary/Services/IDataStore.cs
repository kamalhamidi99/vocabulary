﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Vocabulary.Services
{
    public interface IDataStore<T>
    {
        Task<bool> SaveAsync(T item);
        Task<bool> AddAsync(T item);
        Task<bool> UpdateAsync(T item);
        Task<bool> DeleteAsync(int id);
        Task<T> GetAsync(int id);
        Task<List<T>> GetAllAsync();
    }
}