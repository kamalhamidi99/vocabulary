﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SQLite;
using Vocabulary.Entity;

namespace Vocabulary.Services
{
    public class LessonService : IDataStore<LessonEntity>
    {
        readonly SQLiteAsyncConnection _database;

        public LessonService()
        {
            _database = App.SqLiteAsyncConnection;
            _database.CreateTableAsync<LessonEntity>().Wait();
        }

        public async Task<bool> SaveAsync(LessonEntity item)
        {
            if (item.Id != 0)
                return await UpdateAsync(item);

            return await AddAsync(item);
        }

        public async Task<bool> AddAsync(LessonEntity item)
        {
            var r = await _database.InsertAsync(item);
            return r > 0;
        }

        public async Task<bool> UpdateAsync(LessonEntity item)
        {
            var r = await _database.UpdateAsync(item);
            return r > 0;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var item = await _database.Table<LessonEntity>().Where(i => i.Id == id).FirstOrDefaultAsync();

            var words = await App.WordService.SearchAsync(lessonId: item.Id);
            foreach (var wordEntity in words)
            {
                await App.WordService.DeleteAsync(wordEntity.Id);
            }

            var r = await _database.DeleteAsync(item);
            return r > 0;
        }

        public async Task<LessonEntity> GetAsync(int id)
        {
            return await _database.Table<LessonEntity>().Where(i => i.Id == id).FirstOrDefaultAsync();
        }

        public async Task<LessonEntity> GetAsync(int bookId, int number)
        {
            return await _database.Table<LessonEntity>().Where(i => i.BookId == bookId && i.Number == number).FirstOrDefaultAsync();
        }

        public async Task<List<LessonEntity>> GetAllAsync()
        {
            return await _database.Table<LessonEntity>().ToListAsync();
        }

        public async Task<List<LessonEntity>> GetAllAsync(int bookId)
        {
            return await _database.Table<LessonEntity>().Where(x=>x.BookId == bookId).ToListAsync();
        }
    }
}