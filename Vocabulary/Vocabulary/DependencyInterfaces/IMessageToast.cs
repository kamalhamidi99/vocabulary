﻿namespace Vocabulary.DependencyInterfaces
{
    public interface IMessageToast
    {
        void LongAlert(string message);
        void ShortAlert(string message);
    }
}
