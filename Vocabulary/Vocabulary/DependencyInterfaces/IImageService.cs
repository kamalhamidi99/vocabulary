﻿namespace Vocabulary.DependencyInterfaces
{
    public interface IImageService
    {
        void ResizeImage(string sourceFile, string targetFile, float maxWidth, float maxHeight);
    }
}
