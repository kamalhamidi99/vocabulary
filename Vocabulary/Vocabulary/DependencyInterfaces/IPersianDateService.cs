﻿using System;
using System.Collections.Generic;

namespace Vocabulary.DependencyInterfaces
{
    public interface IPersianDateService
    {
        string GetPersianDate(DateTime dateTime, bool? withTime = true);
        string GetPersianDate(bool? withTime = true);
        IList<string> GetPersianDateArray(DateTime? dateTime = null);
        IDictionary<int, string> GetPersianMonthName();
        string GetPersianMonthName(int month);
    }
}
