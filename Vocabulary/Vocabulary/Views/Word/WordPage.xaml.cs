﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Vocabulary.Extension;
using Vocabulary.Renderers;
using Vocabulary.Views.Collection.SaveLesson;
using Vocabulary.Views.Collection.SaveWord;
using Vocabulary.Views.PlaySound;
using Vocabulary.Views.Translate;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Vocabulary.Views.Word
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WordPage
    {
        private readonly WordViewModel _viewModel;

        public WordPage(int lessonId)
        {
            InitializeComponent();

            BindingContext = _viewModel = new WordViewModel()
            {
                LessonId = lessonId
            };
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            _viewModel.LessonEntity = await App.LessonService.GetAsync(id: _viewModel.LessonId);
            _viewModel.BookEntity = await App.BookService.GetAsync(_viewModel.LessonEntity.BookId);

            await LoadOnAppearing();
        }

        private async Task LoadOnAppearing()
        {
            _viewModel.Loading = true;
            Container.Children.Clear();
            NothingToShowHere.IsVisible = false;

            await Task.Delay(200);

            try
            {
                var items = await App.WordService.SearchAsync(lessonId: _viewModel.LessonEntity.Id);
                if (items.Count == 0)
                {
                    NothingToShowHere.IsVisible = true;
                    _viewModel.Loading = false;
                    return;
                }

                foreach (var item in items)
                {
                    var button = new Button()
                    {
                        BackgroundColor = Color.Transparent,
                    };
                    button.Clicked += async (sender, args) =>
                    {
                        ((Button)sender).IsEnabled = false;
                        await Navigation.PushAsync(new TranslatePage(item.Id));
                    };
                    button.Effects.Add(new LongPressedEffect());
                    LongPressedEffect.SetCommand(button, new Command(async () =>
                    {
                        _viewModel.LoadingWait = true;
                        var word = await App.WordService.GetAsync(item.Id);
                        var bytes = Convert.FromBase64String(word.GoogleData);
                        var json = Encoding.UTF8.GetString(bytes, 0, bytes.Length);
                        var obj = Newtonsoft.Json.JsonConvert.DeserializeObject<JArray>(json);

                        DefinitionContainer.Children.Clear();
                        var stackLayout = new StackLayout();
                        try
                        {
                            stackLayout.Children.Add(FrameTranslations(obj));
                        }
                        catch
                        {
                            // ignored
                        }
                        try
                        {
                            stackLayout.Children.Add(FrameDefinitions(obj));
                        }
                        catch
                        {
                            // ignored
                        }
                        stackLayout.Children.Add(new Button()
                        {
                            BackgroundColor = Color.FromHex("#178A6E"),
                            CornerRadius = 5,
                            Text = "Hide",
                            Margin = new Thickness(20,10,20,20),
                            TextColor = Color.White,
                            Command = new Command(() => { _viewModel.LoadingWait = false; })
                        });
                        DefinitionContainer.Children.Add(new ScrollView()
                        {
                            Content = stackLayout
                        });
                    }));
                    Container.Children.Add(new Frame()
                    {
                        Margin = new Thickness(20, 5, 20, 5),
                        Padding = 0,
                        BackgroundColor = Color.White,
                        BorderColor = Color.FromHex("#707070"),
                        CornerRadius = 5,
                        HasShadow = false,
                        Content = new Grid()
                        {
                            Children =
                            {
                                new Label()
                                {
                                    Text = item.Title,
                                    Margin = new Thickness(15, 0, 15, 0),
                                    TextColor = Color.Black,
                                    LineBreakMode = LineBreakMode.WordWrap,
                                    FontSize = 16,
                                    FontAttributes = FontAttributes.Bold,
                                    VerticalOptions = LayoutOptions.CenterAndExpand,
                                    HorizontalOptions = LayoutOptions.FillAndExpand,
                                },
                                button
                            }
                        },
                    });
                }
                _viewModel.Loading = false;
            }
            catch (Exception e)
            {
                await DisplayAlert("Error", e.Message, "OK");
                _viewModel.Loading = false;
            }
            finally
            {
                _viewModel.Loading = false;
            }

            _viewModel.Loading = false;
        }

        private View FrameDefinitions(JArray obj)
        {
            var list = new List<View>();

            foreach (var groups in obj[12])
            {
                list.Add(new Label()
                {
                    Margin = new Thickness(0, 10, 0, 5),
                    FontSize = 20,
                    Text = groups[0].Value<string>().First().ToString().ToUpper() +
                           groups[0].Value<string>().Substring(1),
                });
                var n = 0;
                foreach (var group in groups[1])
                {
                    ++n;
                    var view = new StackLayout()
                    {
                        Margin = new Thickness(0, 10, 0, 0),
                        Orientation = StackOrientation.Horizontal,
                        Children =
                        {
                            new Frame()
                            {
                                CornerRadius = 200,
                                BorderColor = Color.FromHex("#707070"),
                                Padding = new Thickness(15, 10, 15, 10),
                                VerticalOptions = LayoutOptions.Start,
                                HorizontalOptions = LayoutOptions.Center,
                                Content = new Label()
                                {
                                    FontSize = 14,
                                    FontAttributes = FontAttributes.Bold,
                                    Text = n.ToString(),
                                }
                            },
                            new StackLayout()
                            {
                                Margin = new Thickness(10, 0, 0, 0),
                                Children =
                                {
                                    new Label()
                                    {
                                        FontSize = 18,
                                        TextColor = Color.Black,
                                        Text = group[0].Value<string>(),
                                    },
                                    new Label()
                                    {
                                        FontSize = 18,
                                        Text = group.Count() > 2 ? group[2].Value<string>() : "",
                                    }
                                }
                            }
                        }
                    };
                    list.Add(view);
                }
            }

            var stackLayout = new StackLayout()
            {
                Children =
                {
                    new StackLayout()
                    {
                        Orientation = StackOrientation.Horizontal,
                        Margin = new Thickness(0, 0, 0, 0),
                        Children =
                        {
                            new Image()
                            {
                                Source = "ArrowDownIcon",
                                Margin = new Thickness(0, 5, 10, 0),
                                WidthRequest = 20,
                                Aspect = Aspect.AspectFit,
                            },
                            new Label()
                            {
                                Text = "Definitions",
                                FontSize = 20,
                                FontAttributes = FontAttributes.Bold,
                            }
                        },
                    }
                },
            };

            foreach (var view in list)
            {
                stackLayout.Children.Add(view);
            }

            return new Frame()
            {
                BackgroundColor = Color.White,
                Margin = new Thickness(20, 10, 20, 10),
                CornerRadius = 5,
                Content = stackLayout
            };
        }
        private View FrameTranslations(JArray obj)
        {
            var translate = new Label()
            {
                IsVisible = false,
                Text = obj[0][0][0].Value<string>(),
                FontSize = 20,
                Margin = new Thickness(0, 10, 0, 0),
                FontAttributes = FontAttributes.Bold,
            };
            var translations = new Label()
            {
                Text = "Translations",
                FontSize = 20,
                FontAttributes = FontAttributes.Bold,
            };
            translations.Effects.Add(new LongPressedEffect());
            LongPressedEffect.SetCommand(translations, new Command(() => { translate.IsVisible = true; }));
            var stackLayout = new StackLayout()
            {
                Children =
                {
                    new StackLayout()
                    {
                        Margin = new Thickness(0, 0, 0, 0),
                        Children =
                        {
                            translations,
                            translate
                        },
                    }
                },
            };

            return new Frame()
            {
                BackgroundColor = Color.White,
                Margin = new Thickness(20, 10, 20, 10),
                CornerRadius = 5,
                Content = stackLayout
            };
        }

        private async void MenuClick(object sender, EventArgs e)
        {
            var menu = new List<string>();

            if (_viewModel.BookEntity.Code.IsGuid())
            {
                menu.Add("Edit Lesson");
                menu.Add("Add Word");
            }

            menu.Add("Play audio");

            var action = await DisplayActionSheet(null, null, null,
                menu.ToArray());
            if (action == "Edit Lesson")
            {
                await Navigation.PushAsync(new SaveLessonPage(_viewModel.LessonEntity.Id, _viewModel.LessonEntity.Id));
            }
            if (action == "Add Word")
            {
                await Navigation.PushAsync(new SaveWordPage(_viewModel.BookEntity.Id, _viewModel.LessonEntity.Id));
            }
            if (action == "Play audio")
            {
                await Navigation.PushAsync(new PlaySoundPage(_viewModel.LessonEntity.Id));
            }
        }

        private async void GoToPagePrev(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }

        protected override bool OnBackButtonPressed()
        {
            if (_viewModel.LoadingWait)
            {
                _viewModel.LoadingWait = false;
                return true;
            }

            return base.OnBackButtonPressed();
        }

        private async void NextLesson(object sender, EventArgs e)
        {
            var all = await App.LessonService.GetAllAsync(_viewModel.LessonEntity.BookId);
            var arrays = all.ToArray();
            var index = Array.FindIndex(arrays, x => x.Id == _viewModel.LessonEntity.Id);
            if ((arrays.Length - 1) < (index + 1))
            {
                return;
            }
            _viewModel.LessonEntity = arrays[index + 1];
            await LoadOnAppearing();
        }
        private async void PrevLesson(object sender, EventArgs e)
        {
            var all = await App.LessonService.GetAllAsync(_viewModel.LessonEntity.BookId);
            var arrays = all.ToArray();
            var index = Array.FindIndex(arrays, x => x.Id == _viewModel.LessonEntity.Id);
            if (0 > (index - 1))
            {
                return;
            }
            _viewModel.LessonEntity = arrays[index - 1];
            await LoadOnAppearing();
        }

    }
}