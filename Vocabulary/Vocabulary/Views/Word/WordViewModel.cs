﻿using Vocabulary.Entity;
using Vocabulary.ViewModels;

namespace Vocabulary.Views.Word
{
    public class WordViewModel : BaseViewModel
    {
        public WordViewModel()
        {
        }

        public int BookId { get; set; }
        private BookEntity _bookEntity = new BookEntity();
        public BookEntity BookEntity
        {
            get => _bookEntity;
            set => SetProperty(ref _bookEntity, value);
        }

        public int LessonId { get; set; }
        private LessonEntity _lessonEntity = new LessonEntity();
        public LessonEntity LessonEntity
        {
            get => _lessonEntity;
            set => SetProperty(ref _lessonEntity, value);
        }

    }
}