﻿using Vocabulary.ViewModels;

namespace Vocabulary.Views.Setting
{
    public class SettingViewModel : BaseViewModel
    {

        private bool _playSoundInTranslatePageAutomatically;
        public bool PlaySoundInTranslatePageAutomatically
        {
            get => _playSoundInTranslatePageAutomatically;
            set => SetProperty(ref _playSoundInTranslatePageAutomatically, value);
        }

        private bool _playSoundInTranslatePageAutomaticallyUs;
        public bool PlaySoundInTranslatePageAutomaticallyUs
        {
            get => _playSoundInTranslatePageAutomaticallyUs;
            set => SetProperty(ref _playSoundInTranslatePageAutomaticallyUs, value);
        }

        private bool _playSoundInTranslatePageAutomaticallyUk;
        public bool PlaySoundInTranslatePageAutomaticallyUk
        {
            get => _playSoundInTranslatePageAutomaticallyUk;
            set => SetProperty(ref _playSoundInTranslatePageAutomaticallyUk, value);
        }

    }
}