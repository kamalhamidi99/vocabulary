﻿using System;
using System.Threading.Tasks;
using Vocabulary.Entity;
using Vocabulary.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Vocabulary.Views.Setting
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SettingPage
    {
        private static MainPage RootPage => Application.Current.MainPage as MainPage;
        private readonly SettingViewModel _viewModel;

        public SettingPage()
        {
            InitializeComponent();

            BindingContext = _viewModel = new SettingViewModel();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            var setting = await App.SettingService.GetAsync() ?? new SettingEntity();
            _viewModel.PlaySoundInTranslatePageAutomatically = setting.PlaySoundInTranslatePageAutomatically;
            if (setting.PlaySoundInTranslatePageAutomaticallyType != null)
            {
                if (setting.PlaySoundInTranslatePageAutomaticallyType == SoundType.Uk)
                {
                    _viewModel.PlaySoundInTranslatePageAutomaticallyUk = true;
                    _viewModel.PlaySoundInTranslatePageAutomaticallyUs = false;
                }
                else if (setting.PlaySoundInTranslatePageAutomaticallyType == SoundType.Us)
                {
                    _viewModel.PlaySoundInTranslatePageAutomaticallyUk = false;
                    _viewModel.PlaySoundInTranslatePageAutomaticallyUs = true;
                }
            }
        }

        private async void GoToPagePrev(object sender, EventArgs e)
        {
            await RootPage.NavigateFromMenu((int)MenuItemType.BookPage);
        }

        protected override bool OnBackButtonPressed()
        {
            Task.Run(async () =>
            {
                await RootPage.NavigateFromMenu((int)MenuItemType.BookPage);
            });
            return true;
        }

        private void PlaySoundInTranslatePageAutomaticallyUk_OnToggled(object sender, ToggledEventArgs e)
        {
            _viewModel.PlaySoundInTranslatePageAutomaticallyUs = !_viewModel.PlaySoundInTranslatePageAutomaticallyUk;
        }

        private void PlaySoundInTranslatePageAutomaticallyUs_OnToggled(object sender, ToggledEventArgs e)
        {
            _viewModel.PlaySoundInTranslatePageAutomaticallyUk = !_viewModel.PlaySoundInTranslatePageAutomaticallyUs;
        }

        private async void Save_OnClicked(object sender, EventArgs e)
        {
            var setting = await App.SettingService.GetAsync();
            setting.PlaySoundInTranslatePageAutomatically = _viewModel.PlaySoundInTranslatePageAutomatically;
            if (setting.PlaySoundInTranslatePageAutomatically)
            {
                if (_viewModel.PlaySoundInTranslatePageAutomaticallyUk)
                {
                    setting.PlaySoundInTranslatePageAutomaticallyType = SoundType.Uk;
                }
                else if (_viewModel.PlaySoundInTranslatePageAutomaticallyUs)
                {
                    setting.PlaySoundInTranslatePageAutomaticallyType = SoundType.Us;
                }
            }


            var result = await App.SettingService.SaveAsync(setting);
            if (result)
            {
                await DisplayAlert("Success", "Data saved.", "OK");
            }
            else
            {
                await DisplayAlert("Error", "Can not save data.", "OK");
            }
        }

        private void PlaySoundInTranslatePageAutomatically_OnToggled(object sender, ToggledEventArgs e)
        {
            if (!_viewModel.PlaySoundInTranslatePageAutomaticallyUk && !_viewModel.PlaySoundInTranslatePageAutomaticallyUs)
            {
                _viewModel.PlaySoundInTranslatePageAutomaticallyUs = true;
            }
        }
    }
}