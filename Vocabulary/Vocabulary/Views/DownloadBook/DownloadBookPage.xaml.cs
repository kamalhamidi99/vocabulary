﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Vocabulary.Entity;
using Vocabulary.Extension;
using Vocabulary.Models;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Vocabulary.Views.DownloadBook
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DownloadBookPage
    {
        private readonly DownloadBookViewModel _viewModel;
        private static MainPage RootPage => Application.Current.MainPage as MainPage;

        public DownloadBookPage()
        {
            InitializeComponent();

            BindingContext = _viewModel = new DownloadBookViewModel();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            await LoadOnAppearing();
        }

        private async Task LoadOnAppearing()
        {
            _viewModel.Loading = true;
            Container.Children.Clear();

            try
            {
                if (Connectivity.NetworkAccess != NetworkAccess.Internet)
                {
                    _viewModel.Loading = false;
                    await DisplayAlert("Error", "No internet is available.", "OK");
                    return;
                }

                var httpClient = await HttpClientExtension.SendAndReadAsByteAsync(HttpMethod.Get,
                    "https://drive.google.com/uc?export=download&confirm=iCmK&id=1ia0DP-hAELcZiVYB1qZXYquUhgkogZ5y");
                if (httpClient.StatusCode != HttpStatusCode.OK)
                {
                    _viewModel.Loading = false;
                    await DisplayAlert("Error", "Can not download the book list.", "OK");
                    return;
                }

                var utfString = Encoding.UTF8.GetString(httpClient.ResponseAsByte, 0, httpClient.ResponseAsByte.Length);
                var items = JsonConvert.DeserializeObject<List<BookEntity>>(utfString);

                var getall = await App.BookService.GetAllAsync() ?? new List<BookEntity>();

                foreach (var item in items)
                {
                    var check = getall.FirstOrDefault(x => x.Code.ToLower() == item.Code.ToLower());
                    if (check != null)
                    {
                        if (check.Version == item.Version)
                        {
                            item.IconSource = "CheckGreenIcon";
                            item.IconLabel = "Installed";
                        }
                        else
                        {
                            item.IconSource = "RefreshGreenIcon";
                            item.IconLabel = "Update Exist";
                        }
                    }
                    else
                    {
                        item.IconSource = "DownloadGreenIcon";
                        item.IconLabel = "Download";
                    }

                    item.CoverImage = ImageSource.FromStream(
                        () => new MemoryStream(Convert.FromBase64String(item.Cover)));

                    _viewModel.BookEntities.Add(item);
                    Container.Children.Add(new Frame()
                    {
                        Margin = new Thickness(20, 5, 20, 5),
                        Padding = 5,
                        BackgroundColor = Color.White,
                        BorderColor = Color.FromHex("#707070"),
                        CornerRadius = 5,
                        VerticalOptions = LayoutOptions.Start,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        HasShadow = false,
                        Content = new StackLayout()
                        {
                            Orientation = StackOrientation.Horizontal,
                            Children =
                            {
                                new Frame()
                                {
                                    CornerRadius = 5,
                                    Padding = 0,
                                    VerticalOptions = LayoutOptions.Start,
                                    IsClippedToBounds = true,
                                    HasShadow = false,
                                    Content = new Image()
                                    {
                                        WidthRequest = 60,
                                        HeightRequest = 80,
                                        Aspect = Aspect.Fill,
                                        HorizontalOptions = LayoutOptions.Center,
                                        VerticalOptions = LayoutOptions.Center,
                                        Source = item.CoverImage
                                    }
                                },
                                new StackLayout()
                                {
                                    HorizontalOptions = LayoutOptions.FillAndExpand,
                                    VerticalOptions = LayoutOptions.CenterAndExpand,
                                    Children =
                                    {
                                        new Label()
                                        {
                                            Text = item.Title,
                                            TextColor = Color.FromHex("#707070"),
                                            HorizontalOptions = LayoutOptions.FillAndExpand,
                                            LineBreakMode = LineBreakMode.WordWrap,
                                            FontSize = 20,
                                            FontAttributes = FontAttributes.Bold,
                                        },
                                        new Label()
                                        {
                                            Text = item.Level,
                                            TextColor = Color.FromHex("#707070"),
                                            HorizontalOptions = LayoutOptions.FillAndExpand,
                                            LineBreakMode = LineBreakMode.WordWrap,
                                            Margin = new Thickness(0, -8, 0, 0)
                                        },
                                        new Label()
                                        {
                                            Text = item.Writer,
                                            TextColor = Color.FromHex("#707070"),
                                            HorizontalOptions = LayoutOptions.FillAndExpand,
                                            LineBreakMode = LineBreakMode.WordWrap,
                                            Margin = new Thickness(0, -8, 0, 0)
                                        },
                                        new StackLayout()
                                        {
                                            Orientation = StackOrientation.Horizontal,
                                            HorizontalOptions = LayoutOptions.EndAndExpand,
                                            Children =
                                            {
                                                new Label()
                                                {
                                                    Text = item.IconLabel,
                                                    Margin = new Thickness(0, 0, 5, 0),
                                                    VerticalOptions = LayoutOptions.Center,
                                                    TextColor = Color.FromHex("#10775E")
                                                },
                                                new Image()
                                                {
                                                    Source = item.IconSource,
                                                    Margin = new Thickness(2, 2, 10, 0),
                                                    WidthRequest = 20,
                                                    HeightRequest = 30,
                                                    Aspect = Aspect.AspectFit,
                                                    HorizontalOptions = LayoutOptions.End,
                                                    VerticalOptions = LayoutOptions.End
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        GestureRecognizers =
                        {
                            new TapGestureRecognizer()
                            {
                                Command = new Command(() => OnItemSelected(item.Code))
                            },
                            new TapGestureRecognizer()
                            {
                                NumberOfTapsRequired = 2,
                                Command = new Command(()=>{})
                            }
                        }
                    });
                }

                _viewModel.Loading = false;
            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", ex.Message, "OK");
                _viewModel.Loading = false;
            }
            finally
            {
                _viewModel.Loading = false;
            }
        }

        private async void OnItemSelected(string code)
        {
            try
            {
                _viewModel.LoadingWait = true;
                _viewModel.ProgressLabel = "Preperying";
                _viewModel.Progress = 0;
                ButtonOk.IsVisible = false;

                if (Connectivity.NetworkAccess != NetworkAccess.Internet)
                {
                    _viewModel.LoadingWait = false;
                    await DisplayAlert("Error", "No internet is available.", "OK");
                    return;
                }

                var item = _viewModel.BookEntities.FirstOrDefault(x => x.Code == code);
                if (item == null)
                {
                    _viewModel.LoadingWait = false;
                    await DisplayAlert("Error", "This book is not exist.", "OK");
                    return;
                }

                var get = await App.BookService.GetAsync(item.Code);
                if (get != null)
                {
                    if (get.Version == item.Version)
                    {
                        _viewModel.LoadingWait = false;
                        await DisplayAlert("", "You have already downloaded this book.", "OK");
                        return;
                    }
                    else
                    {
                        await App.BookService.DeleteAsync(get.Id);
                    }
                }

                var destinationFilePath = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + "/download.zip";
                var client = new HttpClientDownloadWithProgressExtension(item.Link, destinationFilePath);
                client.ProgressChanged += (totalFileSize, totalBytesDownloaded, progressPercentage) =>
                {
                    _viewModel.ProgressLabel = "Downloading";
                    _viewModel.Progress = ((progressPercentage ?? 0) / 100);
                };

                await client.StartDownload();

                _viewModel.ProgressLabel = "Installing";

                var encript = "";

                try
                {
                    using (var zip = ZipFile.Open(destinationFilePath, ZipArchiveMode.Update))
                    {
                        foreach (var file in zip.Entries)
                        {
                            encript = new StreamReader(file.Open(), Encoding.UTF8).ReadToEnd();
                        }
                    }
                }
                catch
                {
                    _viewModel.LoadingWait = false;
                    await DisplayAlert("Error", "An error acquire  when trying to get data.", "OK");
                    return;
                }

                if (string.IsNullOrWhiteSpace(encript))
                {
                    _viewModel.LoadingWait = false;
                    await DisplayAlert("Error", "Can not read data.", "OK");
                    return;
                }

                var e = StringCipherExtension.Decrypt(encript);
                var data = JsonConvert.DeserializeObject<BookEntity>(e);

                var addbook = await App.BookService.AddAsync(data);
                if (!addbook)
                {
                    _viewModel.LoadingWait = false;
                    await DisplayAlert("Error", "Can not add the book.", "OK");
                    return;
                }

                var book = await App.BookService.GetAsync(data.Code);

                foreach (var lessonEntity in data.Lessons)
                {
                    lessonEntity.BookId = book.Id;
                    var addlesson = await App.LessonService.AddAsync(lessonEntity);
                    if (!addlesson)
                        continue;

                    var lesson = await App.LessonService.GetAsync(book.Id, lessonEntity.Number);

                    foreach (var wordEntity in lessonEntity.Words)
                    {
                        wordEntity.BookId = book.Id;
                        wordEntity.LessonId = lesson.Id;
                        await App.WordService.AddAsync(wordEntity);
                    }
                }

                if (File.Exists(destinationFilePath)) File.Delete(destinationFilePath);
                _viewModel.ProgressLabel = "Done";
                ButtonOk.IsVisible = true;
            }
            catch (Exception exception)
            {
                _viewModel.LoadingWait = false;
                await DisplayAlert("Error", exception.Message, "OK");
            }

            if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Personal) + "/download.zip"))
            {
                File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.Personal) + "/download.zip");
            }
        }

        private async void GoToPageBack(object sender, EventArgs e)
        {
            await RootPage.NavigateFromMenu((int)MenuItemType.BookPage);
        }

        private async void ButtonOk_OnClicked(object sender, EventArgs e)
        {
            ButtonOk.IsVisible = false;
            _viewModel.LoadingWait = false;
            await LoadOnAppearing();
        }

        protected override bool OnBackButtonPressed()
        {
            Task.Run(async () =>
            {
                await RootPage.NavigateFromMenu((int)MenuItemType.BookPage);
            });
            return true;
        }
    }
}