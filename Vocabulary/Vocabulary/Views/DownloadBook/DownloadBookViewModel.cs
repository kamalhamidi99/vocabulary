﻿using System.Collections.Generic;
using Vocabulary.Entity;
using Vocabulary.ViewModels;

namespace Vocabulary.Views.DownloadBook
{
    public class DownloadBookViewModel : BaseViewModel
    {
        public DownloadBookViewModel()
        {
            BookEntities = new List<BookEntity>();
        }

        public IList<BookEntity> BookEntities { get; set; }

        double _progress;
        public double Progress
        {
            get => _progress;
            set => SetProperty(ref _progress, value);
        }

        string _progressLabel;
        public string ProgressLabel
        {
            get => _progressLabel;
            set => SetProperty(ref _progressLabel, value);
        }
    }
}