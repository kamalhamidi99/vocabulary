﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.TextToSpeech;
using Plugin.TextToSpeech.Abstractions;
using Vocabulary.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Vocabulary.Views.PlaySound
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PlaySoundPage
    {
        private readonly PlaySoundViewModel _viewModel;
        private readonly IList<ViewList> _viewLists;
        CancellationToken _cancellationToken;

        public PlaySoundPage(int lessonId)
        {
            InitializeComponent();

            _viewLists = new List<ViewList>();

            BindingContext = _viewModel = new PlaySoundViewModel()
            {
                LessonId = lessonId,
            };
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            _viewModel.LessonEntity = await App.LessonService.GetAsync(_viewModel.LessonId);
            _viewModel.BookEntity = await App.BookService.GetAsync(_viewModel.LessonEntity.BookId);

            await LoadOnAppearing();
        }

        private async Task LoadOnAppearing()
        {
            _viewModel.Loading = true;
            Container.Children.Clear();

            await Task.Delay(200);

            try
            {
                _viewModel.WordEntities = await App.WordService.SearchAsync(lessonId: _viewModel.LessonEntity.Id);
                if (_viewModel.WordEntities.Count == 0)
                {
                    _viewModel.Loading = false;
                    return;
                }

                foreach (var item in _viewModel.WordEntities)
                {
                    var trans = new Label()
                    {
                        IsVisible = false,
                        Text = item.Translate,
                        FontSize = 16,
                        HorizontalOptions = LayoutOptions.EndAndExpand,
                        HorizontalTextAlignment = TextAlignment.End
                    };
                    _viewLists.Add(new ViewList()
                    {
                        Id = "Translate",
                        View = trans
                    });
                    var view = new Frame()
                    {
                        Padding = 10,
                        Margin = new Thickness(5,0,5,0),
                        BorderColor = Color.FromHex("#178A6E"),
                        HasShadow = false,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        Content = new StackLayout()
                        {
                            Children =
                            {
                                new Label()
                                {
                                    Text = item.Title,
                                    FontSize = 16,
                                    HorizontalOptions = LayoutOptions.StartAndExpand,
                                    HorizontalTextAlignment = TextAlignment.Start
                                },
                                trans
                            }
                        }
                    };
                    Container.Children.Add(view);
                    _viewLists.Add(new ViewList()
                    {
                        View = view,
                        Id = item.Id.ToString(),
                    });
                }

                _viewModel.Loading = false;
            }
            catch (Exception e)
            {
                await DisplayAlert("Error", e.Message, "OK");
                _viewModel.Loading = false;
            }
            finally
            {
                _viewModel.Loading = false;
            }

            _viewModel.Loading = false;
        }

        private async void PlayButton_OnClicked(object sender, EventArgs e)
        {
            if (_viewModel.IsPlay)
            {
                if (!_cancellationToken.IsCancellationRequested)
                {
                    _cancellationToken = new CancellationToken(true);
                }
                _viewModel.IsPlay = false;
            }
            else
            {
                _cancellationToken = new CancellationToken(false);

                if (_viewModel.ShowTranslate)
                {
                    foreach (var item in _viewLists.Where(x=>x.Id == "Translate").ToList())
                    {
                        item.View.IsVisible = true;
                    }
                }
                else
                {
                    foreach (var item in _viewLists.Where(x => x.Id == "Translate").ToList())
                    {
                        item.View.IsVisible = false;
                    }
                }
                _viewModel.ButtonText = "Stop";
                _viewModel.IsPlay = true;
            }

            CrossLocale locale;
            if (_viewModel.SoundUk)
            {
                locale = App.LocaleUk;
            } 
            else if (_viewModel.SoundUs)
            {
                locale = App.LocaleUs;
            }

            try
            {
                foreach (var word in _viewModel.WordEntities)
                {
                    if (_viewModel.IsPlay == false)
                    {
                        break;
                    }

                    var play = _viewLists.FirstOrDefault(x => x.Toggle == "Play");
                    if (play != null)
                    {
                        play.Toggle = "";
                        var frame = (Frame)play.View;
                        frame.BorderColor = Color.FromHex("#178A6E");
                    }

                    var current = _viewLists.FirstOrDefault(x => x.Id == word.Id.ToString());
                    if (current != null)
                    {
                        current.Toggle = "Play";
                        var frame = (Frame)current.View;
                        frame.BorderColor = Color.Red;
                    }

                    await CrossTextToSpeech.Current.Speak(word.Title, locale, _viewModel.Pitch, _viewModel.SpeakRate, _viewModel.Volume, _cancellationToken);

                    if (_viewModel.UseDefinitions)
                    {
                        var all = GetDefinitions(word.GoogleData);
                        if (all.Any())
                        {
                            await Task.Delay(1000, _cancellationToken);
                            await CrossTextToSpeech.Current.Speak("Definitions", locale, _viewModel.Pitch, _viewModel.SpeakRate, _viewModel.Volume, _cancellationToken);
                            foreach (var dic in all)
                            {
                                if (_viewModel.IsPlay == false)
                                {
                                    break;
                                }

                                await Task.Delay(1000, _cancellationToken);
                                await CrossTextToSpeech.Current.Speak(dic.Key, locale, _viewModel.Pitch, _viewModel.SpeakRate, _viewModel.Volume, _cancellationToken);
                                await Task.Delay(1000, _cancellationToken);

                                foreach (var text in dic.Value.Take(_viewModel.NumberOfDefinitions).ToList())
                                {
                                    if (_viewModel.IsPlay == false)
                                    {
                                        break;
                                    }

                                    await CrossTextToSpeech.Current.Speak(text, locale, _viewModel.Pitch, _viewModel.SpeakRate, _viewModel.Volume, _cancellationToken);
                                    await Task.Delay(_viewModel.TimeBetweenDefinitions * 1000, _cancellationToken);
                                }
                            }
                        }
                    }

                    if (_viewModel.UseExamples)
                    {
                        var all = GetExamples(word.GoogleData);
                        if (all.Any())
                        {
                            await Task.Delay(1000, _cancellationToken);
                            await CrossTextToSpeech.Current.Speak("Examples", locale, _viewModel.Pitch, _viewModel.SpeakRate, _viewModel.Volume, _cancellationToken);
                            await Task.Delay(1000, _cancellationToken);
                            foreach (var text in all.Take(_viewModel.NumberOfExamples).ToList())
                            {
                                if (_viewModel.IsPlay == false)
                                {
                                    break;
                                }

                                await CrossTextToSpeech.Current.Speak(text, locale, _viewModel.Pitch, _viewModel.SpeakRate, _viewModel.Volume, _cancellationToken);
                                await Task.Delay(_viewModel.TimeBetweenExamples * 1000, _cancellationToken);
                            }
                        }
                    }

                    await Task.Delay(_viewModel.DelayBetweenWords * 1000, _cancellationToken);
                }
            }
            catch 
            {
                //
            }

            var last = _viewLists.FirstOrDefault(x => x.Toggle == "Play");
            if (last != null)
            {
                last.Toggle = "";
                var frame = (Frame)last.View;
                frame.BorderColor = Color.FromHex("#178A6E");
            }

            _viewModel.ButtonText = "Play";
            _viewModel.IsPlay = false;
        }

        private static IList<string> GetExamples(string translate)
        {
            try
            {
                var bytes = Convert.FromBase64String(translate);
                var json = Encoding.UTF8.GetString(bytes, 0, bytes.Length);
                var obj = JsonConvert.DeserializeObject<JArray>(json);

                var list = new List<string>();

                foreach (var group in obj[13][0])
                {
                    list.Add(Regex.Replace(group[0].Value<string>(), "<.*?>", string.Empty));
                }

                return list;
            }
            catch
            {
                return new List<string>();
            }
        }

        private static IDictionary<string, IList<string>> GetDefinitions(string translate)
        {
            try
            {
                var bytes = Convert.FromBase64String(translate);
                var json = Encoding.UTF8.GetString(bytes, 0, bytes.Length);
                var obj = JsonConvert.DeserializeObject<JArray>(json);

                var dic = new Dictionary<string, IList<string>>();
                foreach (var groups in obj[12])
                {
                    var values = new List<string>();
                    foreach (var group in groups[1])
                    {
                        values.Add(group[0].Value<string>());
                    }

                    dic.Add(groups[0].Value<string>().First().ToString().ToUpper() +
                            groups[0].Value<string>().Substring(1), values);
                }

                return dic;
            }
            catch 
            {
                return new Dictionary<string, IList<string>>();
            }
        }

        private void SoundUkSwitch_OnToggled(object sender, ToggledEventArgs e)
        {
            _viewModel.SoundUs = !_viewModel.SoundUk;
        }

        private void SoundUsSwitch_OnToggled(object sender, ToggledEventArgs e)
        {
            _viewModel.SoundUk = !_viewModel.SoundUs;
        }

        private async void GoToPagePrev(object sender, EventArgs e)
        {
            if (!_cancellationToken.IsCancellationRequested)
            {
                _cancellationToken = new CancellationToken(true);
            }
            _viewModel.IsPlay = false;
            await Navigation.PopAsync();
        }

        protected override bool OnBackButtonPressed()
        {
            if (!_cancellationToken.IsCancellationRequested)
            {
                _cancellationToken = new CancellationToken(true);
            }

            _viewModel.IsPlay = false;

            return base.OnBackButtonPressed();
        }
    }
}