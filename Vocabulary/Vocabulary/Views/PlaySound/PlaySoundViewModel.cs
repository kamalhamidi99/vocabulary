﻿using System.Collections.Generic;
using Vocabulary.Entity;
using Vocabulary.ViewModels;

namespace Vocabulary.Views.PlaySound
{
    public class PlaySoundViewModel : BaseViewModel
    {
        public PlaySoundViewModel()
        {
            DelayBetweenWords = 3;
            UseDefinitions = true;
            NumberOfDefinitions = 1;
            TimeBetweenDefinitions = 2;
            UseExamples = true;
            NumberOfExamples = 1;
            TimeBetweenExamples = 2;
            SoundUk = true;
            ShowTranslate = false;

            Volume = 1.0f;
            SpeakRate = 0.8f;
            Pitch = 1.0f;

            ButtonText = "Play";
        }

        public IList<WordEntity> WordEntities { get; set; }

        public int BookId { get; set; }
        private BookEntity _bookEntity = new BookEntity();
        public BookEntity BookEntity
        {
            get => _bookEntity;
            set => SetProperty(ref _bookEntity, value);
        }

        public int LessonId { get; set; }
        private LessonEntity _lessonEntity = new LessonEntity();
        public LessonEntity LessonEntity
        {
            get => _lessonEntity;
            set => SetProperty(ref _lessonEntity, value);
        }

        public int WordId { get; set; }
        private WordEntity _wordEntity = new WordEntity();
        public WordEntity WordEntity
        {
            get => _wordEntity;
            set => SetProperty(ref _wordEntity, value);
        }

        private string _subtitle;
        public string SubTitle
        {
            get => _subtitle;
            set => SetProperty(ref _subtitle, value);
        }

        private int _delayBetweenWords;
        public int DelayBetweenWords
        {
            get => _delayBetweenWords;
            set => SetProperty(ref _delayBetweenWords, value);
        }

        private bool _soundUs;
        public bool SoundUs
        {
            get => _soundUs;
            set => SetProperty(ref _soundUs, value);
        }

        private bool _soundUk;
        public bool SoundUk
        {
            get => _soundUk;
            set => SetProperty(ref _soundUk, value);
        }

        private bool _useDefinitions;
        public bool UseDefinitions
        {
            get => _useDefinitions;
            set => SetProperty(ref _useDefinitions, value);
        }

        private int _numberOfDefinitions;
        public int NumberOfDefinitions
        {
            get => _numberOfDefinitions;
            set => SetProperty(ref _numberOfDefinitions, value);
        }

        private int _timeBetweenDefinitions;
        public int TimeBetweenDefinitions
        {
            get => _timeBetweenDefinitions;
            set => SetProperty(ref _timeBetweenDefinitions, value);
        }

        private bool _useExamples;
        public bool UseExamples
        {
            get => _useExamples;
            set => SetProperty(ref _useExamples, value);
        }

        private int _numberOfExamples;
        public int NumberOfExamples
        {
            get => _numberOfExamples;
            set => SetProperty(ref _numberOfExamples, value);
        }

        private int _timeBetweenExamples;
        public int TimeBetweenExamples
        {
            get => _timeBetweenExamples;
            set => SetProperty(ref _timeBetweenExamples, value);
        }


        private string _buttonText;
        public string ButtonText
        {
            get => _buttonText;
            set => SetProperty(ref _buttonText, value);
        }

        private bool _isPlay;
        public bool IsPlay
        {
            get => _isPlay;
            set => SetProperty(ref _isPlay, value);
        }

        private float _pitch;
        public float Pitch
        {
            get => _pitch;
            set => SetProperty(ref _pitch, value);
        }

        private float _speakRate;
        public float SpeakRate
        {
            get => _speakRate;
            set => SetProperty(ref _speakRate, value);
        }

        private float _volume;
        public float Volume
        {
            get => _volume;
            set => SetProperty(ref _volume, value);
        }

        private bool _showTranslate;
        public bool ShowTranslate
        {
            get => _showTranslate;
            set => SetProperty(ref _showTranslate, value);
        }

    }
}