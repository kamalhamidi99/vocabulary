﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Vocabulary.Entity;
using Vocabulary.ViewModels;
using Xamarin.Forms;

namespace Vocabulary.Views.Lesson
{
    public class LessonViewModel : BaseViewModel
    {
        public LessonViewModel()
        {
            Items = new ObservableCollection<LessonEntity>();
            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());
        }

        public ObservableCollection<LessonEntity> Items { get; set; }
        public Command LoadItemsCommand { get; set; }


        public int BookId { get; set; }

        private BookEntity _bookEntity = new BookEntity();
        public BookEntity BookEntity
        {
            get => _bookEntity;
            set => SetProperty(ref _bookEntity, value);
        }

        async Task ExecuteLoadItemsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            var book = await App.BookService.GetAsync(BookId);
            book.CoverImage = ImageSource.FromStream(
                () => new MemoryStream(Convert.FromBase64String(book.Cover)));
            BookEntity = book;

            try
            {
                Items.Clear();
                var items = await App.LessonService.GetAllAsync(BookId);
                foreach (var item in items)
                {
                    Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

    }
}