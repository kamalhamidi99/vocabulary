﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Vocabulary.Extension;
using Vocabulary.Views.Collection.EditBook;
using Vocabulary.Views.Collection.SaveLesson;
using Vocabulary.Views.Word;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Vocabulary.Views.Lesson
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LessonPage
    {
        readonly LessonViewModel _viewModel;

        public LessonPage(int bookId)
        {
            InitializeComponent();

            BindingContext = _viewModel = new LessonViewModel()
            {
                BookId = bookId,
            };
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            _viewModel.BookEntity = await App.BookService.GetAsync(_viewModel.BookId);

            await LoadOnAppearing();
        }

        private async Task LoadOnAppearing()
        {
            Container.Children.Clear();
            _viewModel.Loading = true;
            NothingToShowHere.IsVisible = false;

            await Task.Delay(200);

            var lessons = await App.LessonService.GetAllAsync(_viewModel.BookId);
            if (lessons.Count == 0)
            {
                NothingToShowHere.IsVisible = true;
                _viewModel.Loading = false;
                return;
            }

            foreach (var lesson in lessons)
            {
                var button = new Button()
                {
                    BackgroundColor = Color.Transparent,
                };
                //button.Effects.Add(new LongPressedEffect());
                //LongPressedEffect.SetCommand(button, new Command(() =>
                //{
                //}));
                button.Clicked += async (sender, args) =>
                {
                    ((Button)sender).IsEnabled = false;
                    await Navigation.PushAsync(new WordPage(lesson.Id));
                };

                var view = new Frame()
                {
                    Margin = new Thickness(20, 5, 20, 5),
                    Padding = 0,
                    BackgroundColor = Color.White,
                    BorderColor = Color.FromHex("#707070"),
                    CornerRadius = 5,
                    HasShadow = false,
                    Content = new Grid()
                    {
                        Children =
                        {
                            new StackLayout()
                            {
                                Orientation = StackOrientation.Horizontal,
                                Children =
                                {
                                    new Frame()
                                    {
                                        CornerRadius = 5,
                                        Padding = new Thickness(15,5,15,5),
                                        Margin = new Thickness(10,10,0,10),
                                        BorderColor = Color.FromHex("#178A6E"),
                                        WidthRequest = 40,
                                        HasShadow = false,
                                        Content = new Label()
                                        {
                                            TextColor = Color.FromHex("#178A6E"),
                                            Text = lesson.Number.ToString(),
                                            VerticalOptions = LayoutOptions.CenterAndExpand,
                                            HorizontalOptions = LayoutOptions.CenterAndExpand,
                                            FontSize = 20,
                                            FontAttributes = FontAttributes.Bold,
                                        }
                                    },
                                    new Label()
                                    {
                                        Text = lesson.Title,
                                        Margin = new Thickness(10, 0, 10, 0),
                                        TextColor = Color.Black,
                                        VerticalOptions = LayoutOptions.Center,
                                        LineBreakMode = LineBreakMode.WordWrap,
                                        FontSize = 16,
                                        FontAttributes = FontAttributes.Bold,
                                        HorizontalOptions = LayoutOptions.StartAndExpand,
                                        HorizontalTextAlignment = TextAlignment.Start
                                    }
                                }
                            },
                            button
                        }
                    },
                };

                Container.Children.Add(view);
            }

            _viewModel.Loading = false;
        }

        private async void MenuClick(object sender, EventArgs e)
        {
            var menu = new List<string>
            {
                "Delete Book"
            };

            if (_viewModel.BookEntity.Code.IsGuid())
            {
                menu.Add("Edit Book");
                menu.Add("Add Lesson");
            }

            var action = await DisplayActionSheet(null, null, null,
                menu.ToArray());
            if (action == "Delete Book")
            {
                _viewModel.LoadingWait = true;
                var result = await App.BookService.DeleteAsync(_viewModel.BookId);
                if (!result)
                {
                    await DisplayAlert("Error", "Can not delete this book.", "OK");
                    return;
                }
                _viewModel.LoadingWait = false;

                await Navigation.PopAsync();
            }
            if (action == "Edit Book")
            {
                await Navigation.PushAsync(new EditBookPage(_viewModel.BookEntity.Id));
            }
            if (action == "Add Lesson")
            {
                await Navigation.PushAsync(new SaveLessonPage(_viewModel.BookEntity.Id));
            }
        }

        private async void GoToPagePrev(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }
    }
}