﻿using Vocabulary.ViewModels;

namespace Vocabulary.Views.Collection.AddBook
{
    public class AddBookViewModel : BaseViewModel
    {

        string _bookTitle;
        public string BookTitle
        {
            get => _bookTitle;
            set => SetProperty(ref _bookTitle, value);
        }

        string _bookLevel;
        public string BookLevel
        {
            get => _bookLevel;
            set => SetProperty(ref _bookLevel, value);
        }

        string _bookWriter;
        public string BookWriter
        {
            get => _bookWriter;
            set => SetProperty(ref _bookWriter, value);
        }

        public string BookCoverBase64 { get; set; }
    }
}