﻿using System;
using System.IO;
using System.Threading.Tasks;
using Plugin.Media;
using Vocabulary.DependencyInterfaces;
using Vocabulary.Entity;
using Vocabulary.Extension;
using Vocabulary.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Vocabulary.Views.Collection.AddBook
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddBookPage
    {
        private readonly AddBookViewModel _viewModel;
        private static MainPage RootPage => Application.Current.MainPage as MainPage;
        
        public AddBookPage()
        {
            InitializeComponent();

            BindingContext = _viewModel = new AddBookViewModel();
        }

        private async void GoToPageBack(object sender, EventArgs e)
        {
            Clear();
            await RootPage.NavigateFromMenu((int)MenuItemType.BookPage);
        }

        private async void Add_OnClicked(object sender, EventArgs e)
        {
            var button = ((Button)sender);
            button.IsEnabled = false;
            if (string.IsNullOrWhiteSpace(_viewModel.BookTitle))
            {
                await DisplayAlert("Error", "Enter title.", "OK");
                button.IsEnabled = true;
                return;
            }
            if (string.IsNullOrWhiteSpace(_viewModel.BookWriter))
            {
                await DisplayAlert("Error", "Enter writer.", "OK");
                button.IsEnabled = true;
                return;
            }
            if (string.IsNullOrWhiteSpace(_viewModel.BookLevel))
            {
                await DisplayAlert("Error", "Enter level.", "OK");
                button.IsEnabled = true;
                return;
            }

            var result = await App.BookService.AddAsync(new BookEntity()
            {
                Title = _viewModel.BookTitle,
                Level = _viewModel.BookLevel,
                Writer = _viewModel.BookWriter,
                Cover = _viewModel.BookCoverBase64,
                Code = Guid.NewGuid().ToString(),
                Version = 1,
                Link = string.Empty,
            });
            if (!result)
            {
                await DisplayAlert("Error", "Can not add a new book.", "OK");
                button.IsEnabled = true;
                return;
            }

            await DisplayAlert("Success", "Book added.", "OK");

            Clear();
            button.IsEnabled = true;

            await RootPage.NavigateFromMenu((int)MenuItemType.BookPage);
        }

        private async void FromCamera_OnClicked(object sender, EventArgs e)
        {
            FromCamera.IsEnabled = false;

            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await DisplayAlert("No Camera", ":( No camera available.", "OK");
                FromCamera.IsEnabled = true;
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                Directory = "Photo",
                Name = "cover.jpg"
            });

            if (file == null)
            {
                FromCamera.IsEnabled = true;
                return;
            }

            var imageService = DependencyService.Get<IImageService>();
            imageService.ResizeImage(file.Path, file.Path, 80, 100);

            FromCamera.IsEnabled = true;

            ImageCover.Source = ImageSource.FromStream(() => file.GetStream());
            _viewModel.BookCoverBase64 = file.GetStream().ConvertToBase64();

            File.Delete(file.Path);
        }

        private async void FromGallery_OnClicked(object sender, EventArgs e)
        {
            FromGallery.IsEnabled = false;

            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsPickPhotoSupported)
            {
                await DisplayAlert("No Support", ":( No support for gallery available.", "OK");
                FromGallery.IsEnabled = true;
                return;
            }

            var file = await CrossMedia.Current.PickPhotoAsync();

            if (file == null)
            {
                FromGallery.IsEnabled = true;
                return;
            }

            var imageService = DependencyService.Get<IImageService>();
            var targetFile = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "/cover.png";
            imageService.ResizeImage(file.Path, targetFile, 80, 100);

            FromGallery.IsEnabled = true;

            ImageCover.Source = ImageSource.FromFile(targetFile);

            FileStream fs = new FileStream(targetFile, FileMode.Open, FileAccess.Read);
            StreamReader r = new StreamReader(fs);
            _viewModel.BookCoverBase64 = r.BaseStream.ConvertToBase64();

            File.Delete(targetFile);
        }

        private void Clear()
        {
            _viewModel.BookTitle = _viewModel.BookLevel = _viewModel.BookWriter = _viewModel.BookCoverBase64 = string.Empty;
            ImageCover.Source = "";
        }

        protected override bool OnBackButtonPressed()
        {
            Clear();
            Task.Run(async () =>
            {
                await RootPage.NavigateFromMenu((int)MenuItemType.BookPage);
            });
            return true;
        }
    }
}