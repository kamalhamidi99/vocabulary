﻿using System;
using System.Threading.Tasks;
using Vocabulary.Entity;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Vocabulary.Views.Collection.SaveWord
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SaveWordPage
    {
        private readonly SaveWordViewModel _viewModel;
        
        public SaveWordPage(int bookId, int lessonId, int? wordId = null)
        {
            InitializeComponent();

            BindingContext = _viewModel = new SaveWordViewModel()
            {
                BookId = bookId,
                LessonId = lessonId,
                WordId = wordId
            };
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            if (_viewModel.WordId != null)
            {
                var word = await App.WordService.GetAsync(id: _viewModel.WordId.Value);
                _viewModel.WordTitle = word.Title;
                _viewModel.WordTranslate = word.Translate;
                _viewModel.WordPronounce = word.Pronounce;
            }
        }

        private async void GoToPageBack(object sender, EventArgs e)
        {
            _viewModel.WordPronounce = _viewModel.WordTitle = _viewModel.WordTranslate = string.Empty;
            await Navigation.PopAsync();
        }

        private async void Save_OnClicked(object sender, EventArgs e)
        {
            var button = ((Button)sender);
            button.IsEnabled = false;
            if (string.IsNullOrWhiteSpace(_viewModel.WordTitle))
            {
                await DisplayAlert("Error", "Enter title.", "OK");
                button.IsEnabled = true;
                return;
            }
            if (string.IsNullOrWhiteSpace(_viewModel.WordTranslate))
            {
                await DisplayAlert("Error", "Enter translate.", "OK");
                button.IsEnabled = true;
                return;
            } 
            if (string.IsNullOrWhiteSpace(_viewModel.WordPronounce))
            {
                await DisplayAlert("Error", "Enter pronounce.", "OK");
                button.IsEnabled = true;
                return;
            } 

            if (_viewModel.WordId == null)
            {
                var result = await App.WordService.AddAsync(new WordEntity()
                {
                    BookId = _viewModel.BookId,
                    LessonId = _viewModel.LessonId,
                    Title = _viewModel.WordTitle,
                    Translate = _viewModel.WordTranslate,
                    Pronounce = _viewModel.WordPronounce,
                });
                if (!result)
                {
                    await DisplayAlert("Error", "Can not add a new word.", "OK");
                    button.IsEnabled = true;
                    return;
                }
                await DisplayAlert("Success", "Word added.", "OK");
            }

            if (_viewModel.WordId != null)
            {
                var word = await App.WordService.GetAsync(id: _viewModel.WordId.Value);
                word.Title = _viewModel.WordTitle;
                word.Translate = _viewModel.WordTranslate;
                word.Pronounce = _viewModel.WordPronounce;

                var result = await App.WordService.UpdateAsync(word);
                if (!result)
                {
                    await DisplayAlert("Error", "Can not edit word.", "OK");
                    button.IsEnabled = true;
                    return;
                }
                await DisplayAlert("Success", "Word edited.", "OK");
            }

            _viewModel.WordPronounce = _viewModel.WordTitle = _viewModel.WordTranslate = string.Empty;
            button.IsEnabled = true;

            await Navigation.PopAsync();
        }

        protected override bool OnBackButtonPressed()
        {
            _viewModel.WordPronounce = _viewModel.WordTitle = _viewModel.WordTranslate = string.Empty;
            Task.Run(async () =>
            {
                await Navigation.PopAsync();
            });
            return true;
        }
    }
}