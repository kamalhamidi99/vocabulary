﻿using Vocabulary.ViewModels;

namespace Vocabulary.Views.Collection.SaveWord
{
    public class SaveWordViewModel : BaseViewModel
    {
        public int BookId { get; set; }
        public int LessonId { get; set; }
        public int? WordId { get; set; }

        string _wordTitle;
        public string WordTitle
        {
            get => _wordTitle;
            set => SetProperty(ref _wordTitle, value);
        }

        string _wordTranslate;
        public string WordTranslate
        {
            get => _wordTranslate;
            set => SetProperty(ref _wordTranslate, value);
        }

        string _wordPronounce;
        public string WordPronounce
        {
            get => _wordPronounce;
            set => SetProperty(ref _wordPronounce, value);
        }
    }
}