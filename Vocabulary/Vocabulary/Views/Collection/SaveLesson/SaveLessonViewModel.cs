﻿using Vocabulary.ViewModels;

namespace Vocabulary.Views.Collection.SaveLesson
{
    public class SaveLessonViewModel : BaseViewModel
    {
        public int BookId { get; set; }
        public int? LessonId { get; set; }

        string _lessonTitle;
        public string LessonTitle
        {
            get => _lessonTitle;
            set => SetProperty(ref _lessonTitle, value);
        }

        string _lessonNumber;
        public string LessonNumber
        {
            get => _lessonNumber;
            set => SetProperty(ref _lessonNumber, value);
        }
    }
}