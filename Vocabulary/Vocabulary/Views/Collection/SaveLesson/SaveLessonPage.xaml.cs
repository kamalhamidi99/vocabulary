﻿using System;
using System.Threading.Tasks;
using Vocabulary.Entity;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Vocabulary.Views.Collection.SaveLesson
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SaveLessonPage
    {
        private readonly SaveLessonViewModel _viewModel;
        
        public SaveLessonPage(int bookId, int? lessonId = null)
        {
            InitializeComponent();

            BindingContext = _viewModel = new SaveLessonViewModel()
            {
                BookId = bookId,
                LessonId = lessonId
            };
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            if (_viewModel.LessonId != null)
            {
                var lesson = await App.LessonService.GetAsync(id: _viewModel.LessonId.Value);
                _viewModel.LessonNumber = lesson.Number.ToString();
                _viewModel.LessonTitle = lesson.Title;
            }
        }

        private async void GoToPageBack(object sender, EventArgs e)
        {
            _viewModel.LessonTitle = _viewModel.LessonNumber = string.Empty;
            await Navigation.PopAsync();
        }

        private async void Save_OnClicked(object sender, EventArgs e)
        {
            var button = ((Button)sender);
            button.IsEnabled = false;
            if (string.IsNullOrWhiteSpace(_viewModel.LessonTitle))
            {
                await DisplayAlert("Error", "Enter title.", "OK");
                button.IsEnabled = true;
                return;
            }
            if (string.IsNullOrWhiteSpace(_viewModel.LessonNumber))
            {
                await DisplayAlert("Error", "Enter number.", "OK");
                button.IsEnabled = true;
                return;
            } 
            else
            {
                if (!int.TryParse(_viewModel.LessonNumber, out _))
                {
                    await DisplayAlert("Error", "Enter number.", "OK");
                    button.IsEnabled = true;
                    return;
                }
            }

            if (_viewModel.LessonId == null)
            {
                var result = await App.LessonService.AddAsync(new LessonEntity()
                {
                    BookId = _viewModel.BookId,
                    Title = _viewModel.LessonTitle,
                    Number = int.Parse(_viewModel.LessonNumber),
                });
                if (!result)
                {
                    await DisplayAlert("Error", "Can not add a new lesson.", "OK");
                    button.IsEnabled = true;
                    return;
                }
                await DisplayAlert("Success", "Lesson added.", "OK");
            }

            if (_viewModel.LessonId != null)
            {
                var lesson = await App.LessonService.GetAsync(id: _viewModel.LessonId.Value);
                lesson.Title = _viewModel.LessonTitle;
                lesson.Number = int.Parse(_viewModel.LessonNumber);

                var result = await App.LessonService.UpdateAsync(lesson);
                if (!result)
                {
                    await DisplayAlert("Error", "Can not edit lesson.", "OK");
                    button.IsEnabled = true;
                    return;
                }
                await DisplayAlert("Success", "Lesson edited.", "OK");
            }

            _viewModel.LessonTitle = _viewModel.LessonNumber = string.Empty;
            button.IsEnabled = true;

            await Navigation.PopAsync();
        }

        protected override bool OnBackButtonPressed()
        {
            _viewModel.LessonTitle = _viewModel.LessonNumber = string.Empty;
            Task.Run(async () =>
            {
                await Navigation.PopAsync();
            });
            return true;
        }
    }
}