﻿using Vocabulary.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using Vocabulary.Extension;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Vocabulary.Views
{
    [DesignTimeVisible(false)]
    public partial class MenuPage 
    {
        private static MainPage RootPage => Application.Current.MainPage as MainPage;

        public MenuPage()
        {
            InitializeComponent();
        }

        private async void GoToPageDownloadBook(object sender, EventArgs e)
        {
            await RootPage.NavigateFromMenu((int)MenuItemType.DownloadBook);
        }

        private async void ShareButton_OnClicked(object sender, EventArgs e)
        {
            await Share.RequestAsync(new ShareTextRequest
            {
                Text = "Download vocabulary app from the below link: https://bit.ly/2JqZnfW",
                Title = "Share Vocabulary App"
            });
        }

        private async void CheckForUpdateButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                if (Connectivity.NetworkAccess != NetworkAccess.Internet)
                {
                    await DisplayAlert("Error", "No internet is available.", "OK");
                    return;
                }

                var httpClient = await HttpClientExtension.SendAndReadAsByteAsync(HttpMethod.Get,
                    "https://drive.google.com/uc?export=download&confirm=iCmK&id=18mUiAZKLm2RUbi0lutigpNk5Y8iH26ab");
                if (httpClient.StatusCode != HttpStatusCode.OK)
                {
                    await DisplayAlert("Error", "Can not download the data.", "OK");
                    return;
                }

                var utfString = Encoding.UTF8.GetString(httpClient.ResponseAsByte, 0, httpClient.ResponseAsByte.Length);
                var items = JsonConvert.DeserializeObject<List<CheckForUpdate>>(utfString);
                if (!items.Any())
                {
                    await DisplayAlert("Error", "There is no update available.", "OK");
                    return;
                }

                var update = items.Where(x => int.Parse(x.Version) > int.Parse(AppInfo.BuildString))
                    .OrderByDescending(x => int.Parse(x.Version)).FirstOrDefault();
                if (update != null)
                {
                    var action = await DisplayAlert("Update", "An update is available, Do you want to download it?",
                        "Yes", "No");
                    if (action)
                    {
                        await Browser.OpenAsync(update.Link, BrowserLaunchMode.SystemPreferred);
                    }
                }
                else
                {
                    await DisplayAlert("", "You are up to date.", "OK");
                }
            }
            catch
            {
                await DisplayAlert("Error", "An error aquire.", "OK");
            }
        }

        private async void GoToPageSearch_OnClicked(object sender, EventArgs e)
        {
            await RootPage.NavigateFromMenu((int)MenuItemType.SearchPage);
        }

        private async void GoToPageAddBook(object sender, EventArgs e)
        {
            await RootPage.NavigateFromMenu((int)MenuItemType.AddBook);
        }

        private async void GoToPageSetting(object sender, EventArgs e)
        {
            await RootPage.NavigateFromMenu((int)MenuItemType.Setting);
        }
    }
}