﻿using System.Threading.Tasks;
using Vocabulary.Entity;
using Vocabulary.ViewModels;

namespace Vocabulary.Views.Search
{
    public class SearchViewModel : BaseViewModel
    {
        public SearchViewModel()
        {
        }

        public int? BookId { get; set; }
        private BookEntity _bookEntity = new BookEntity();
        public BookEntity BookEntity
        {
            get => _bookEntity;
            set => SetProperty(ref _bookEntity, value);
        }

        private string _subtitle;
        public string SubTitle
        {
            get => _subtitle;
            set => SetProperty(ref _subtitle, value);
        }

        private string _search;
        public string Search
        {
            get => _search;
            set => SetProperty(ref _search, value);
        }

        private bool _loadingPage;
        public bool LoadingPage
        {
            get => _loadingPage;
            set => SetProperty(ref _loadingPage, value);
        }

        public int PageNumber { get; set; }
    }
}