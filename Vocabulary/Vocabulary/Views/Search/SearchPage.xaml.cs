﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Vocabulary.Models;
using Vocabulary.Renderers;
using Vocabulary.Views.Translate;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Vocabulary.Views.Search
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchPage
    {
        private readonly SearchViewModel _viewModel;
        private static MainPage RootPage => Application.Current.MainPage as MainPage;

        public SearchPage(int? bookId = null)
        {
            InitializeComponent();

            BindingContext = _viewModel = new SearchViewModel()
            {
                BookId = bookId,
            };
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (string.IsNullOrWhiteSpace(_viewModel.Search))
            {
                Container.Children.Clear();
                _viewModel.LoadingPage = false;
            }
        }

        private async void GoToPagePrev(object sender, EventArgs e)
        {
            await RootPage.NavigateFromMenu((int)MenuItemType.BookPage);
        }

        private async Task MakeContain(int pageNumber)
        {
            var words = await App.WordService.SearchAsync(page: pageNumber, word: _viewModel.Search);
            foreach (var word in words)
            {
                var button = new Button()
                {
                    BackgroundColor = Color.Transparent,
                };
                button.Clicked += async (senderButton, args) =>
                {
                    ((Button)senderButton).IsEnabled = false;
                    await Navigation.PushAsync(new TranslatePage(word.Id));
                };
                button.Effects.Add(new LongPressedEffect());
                LongPressedEffect.SetCommand(button, new Command(() =>
                {
                    _viewModel.LoadingWait = true;
                    var bytes = Convert.FromBase64String(word.GoogleData);
                    var json = Encoding.UTF8.GetString(bytes, 0, bytes.Length);
                    var obj = Newtonsoft.Json.JsonConvert.DeserializeObject<JArray>(json);

                    DefinitionContainer.Children.Clear();
                    var stackLayout = new StackLayout();
                    try
                    {
                        stackLayout.Children.Add(FrameTranslations(obj));
                    }
                    catch
                    {
                        // ignored
                    }
                    try
                    {
                        stackLayout.Children.Add(FrameDefinitions(obj));
                    }
                    catch
                    {
                        // ignored
                    }
                    stackLayout.Children.Add(new Button()
                    {
                        BackgroundColor = Color.FromHex("#178A6E"),
                        CornerRadius = 5,
                        Text = "Hide",
                        Margin = new Thickness(20, 10, 20, 20),
                        TextColor = Color.White,
                        Command = new Command(() => { _viewModel.LoadingWait = false; })
                    });
                    DefinitionContainer.Children.Add(new ScrollView()
                    {
                        Content = stackLayout
                    });
                }));
                Container.Children.Add(new Frame()
                {
                    Margin = new Thickness(20, 5, 20, 5),
                    Padding = 0,
                    BackgroundColor = Color.White,
                    BorderColor = Color.FromHex("#707070"),
                    CornerRadius = 5,
                    HasShadow = false,
                    Content = new Grid()
                    {
                        Children =
                        {
                            new Label()
                            {
                                Text = word.Title,
                                Margin = new Thickness(15, 0, 15, 0),
                                TextColor = Color.Black,
                                LineBreakMode = LineBreakMode.WordWrap,
                                FontSize = 16,
                                FontAttributes = FontAttributes.Bold,
                                VerticalOptions = LayoutOptions.CenterAndExpand,
                                HorizontalOptions = LayoutOptions.FillAndExpand,
                            },
                            button
                        }
                    },
                });
            }
        }

        private async void ScrollView_OnScrolled(object sender, ScrolledEventArgs e)
        {
            var scrollView = (ScrollView)sender;
            try
            {
                var contentSize = scrollView.ContentSize.Height;
                var maxPos = contentSize - scrollView.Height;

                if (e.ScrollY.ToString("#####") == maxPos.ToString("#####"))
                {
                    _viewModel.LoadingPage = true;
                    await Task.Delay(1000);
                    _viewModel.PageNumber += 1;
                    await MakeContain(_viewModel.PageNumber);
                    _viewModel.LoadingPage = false;
                }
                else
                {
                    //_viewModel.LoadingPage = false;
                }
                Debug.WriteLine($"Scrolled to pos {e.ScrollY:#####}, max: {maxPos:#####}");
            }
            catch
            {
                //
            }
        }

        private async void DoSearch_OnClicked(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(_viewModel.Search))
            {
                _viewModel.Loading = false;
                return;
            }

            _viewModel.PageNumber = 1;
            _viewModel.Loading = true;
            Container.Children.Clear();

            await MakeContain(_viewModel.PageNumber);

            _viewModel.Loading = false;
        }

        private View FrameDefinitions(JArray obj)
        {
            var list = new List<View>();

            foreach (var groups in obj[12])
            {
                list.Add(new Label()
                {
                    Margin = new Thickness(0, 10, 0, 5),
                    FontSize = 20,
                    Text = groups[0].Value<string>().First().ToString().ToUpper() +
                           groups[0].Value<string>().Substring(1),
                });
                var n = 0;
                foreach (var group in groups[1])
                {
                    ++n;
                    var view = new StackLayout()
                    {
                        Margin = new Thickness(0, 10, 0, 0),
                        Orientation = StackOrientation.Horizontal,
                        Children =
                        {
                            new Frame()
                            {
                                CornerRadius = 200,
                                BorderColor = Color.FromHex("#707070"),
                                Padding = new Thickness(15, 10, 15, 10),
                                VerticalOptions = LayoutOptions.Start,
                                HorizontalOptions = LayoutOptions.Center,
                                Content = new Label()
                                {
                                    FontSize = 14,
                                    FontAttributes = FontAttributes.Bold,
                                    Text = n.ToString(),
                                }
                            },
                            new StackLayout()
                            {
                                Margin = new Thickness(10, 0, 0, 0),
                                Children =
                                {
                                    new Label()
                                    {
                                        FontSize = 18,
                                        TextColor = Color.Black,
                                        Text = group[0].Value<string>(),
                                    },
                                    new Label()
                                    {
                                        FontSize = 18,
                                        Text = group.Count() > 2 ? group[2].Value<string>() : "",
                                    }
                                }
                            }
                        }
                    };
                    list.Add(view);
                }
            }

            var stackLayout = new StackLayout()
            {
                Children =
                {
                    new StackLayout()
                    {
                        Orientation = StackOrientation.Horizontal,
                        Margin = new Thickness(0, 0, 0, 0),
                        Children =
                        {
                            new Image()
                            {
                                Source = "ArrowDownIcon",
                                Margin = new Thickness(0, 5, 10, 0),
                                WidthRequest = 20,
                                Aspect = Aspect.AspectFit,
                            },
                            new Label()
                            {
                                Text = "Definitions",
                                FontSize = 20,
                                FontAttributes = FontAttributes.Bold,
                            }
                        },
                    }
                },
            };

            foreach (var view in list)
            {
                stackLayout.Children.Add(view);
            }

            return new Frame()
            {
                BackgroundColor = Color.White,
                Margin = new Thickness(20, 10, 20, 10),
                CornerRadius = 5,
                Content = stackLayout
            };
        }
        private View FrameTranslations(JArray obj)
        {
            var translate = new Label()
            {
                IsVisible = false,
                Text = obj[0][0][0].Value<string>(),
                FontSize = 20,
                Margin = new Thickness(0, 10, 0, 0),
                FontAttributes = FontAttributes.Bold,
            };
            var translations = new Label()
            {
                Text = "Translations",
                FontSize = 20,
                FontAttributes = FontAttributes.Bold,
            };
            translations.Effects.Add(new LongPressedEffect());
            LongPressedEffect.SetCommand(translations, new Command(() => { translate.IsVisible = true; }));
            var stackLayout = new StackLayout()
            {
                Children =
                {
                    new StackLayout()
                    {
                        Margin = new Thickness(0, 0, 0, 0),
                        Children =
                        {
                            translations,
                            translate
                        },
                    }
                },
            };

            return new Frame()
            {
                BackgroundColor = Color.White,
                Margin = new Thickness(20, 10, 20, 10),
                CornerRadius = 5,
                Content = stackLayout
            };
        }

        protected override bool OnBackButtonPressed()
        {
            if (_viewModel.LoadingWait)
            {
                _viewModel.LoadingWait = false;
                return true;
            }

            Container.Children.Clear();
            _viewModel.Search = "";

            Task.Run(async () =>
            {
                await RootPage.NavigateFromMenu((int)MenuItemType.BookPage);
            });
            return true;
        }

    }
}