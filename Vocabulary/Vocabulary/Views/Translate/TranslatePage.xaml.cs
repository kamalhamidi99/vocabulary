﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Plugin.TextToSpeech;
using Vocabulary.Entity;
using Vocabulary.Extension;
using Vocabulary.Models;
using Vocabulary.Views.Collection.SaveWord;
using Vocabulary.Views.PlaySound;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Vocabulary.Views.Translate
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TranslatePage
    {
        private readonly TranslateViewModel _viewModel;
        private readonly IList<ViewList> _viewLists;

        public TranslatePage(int wordId)
        {
            InitializeComponent();

            _viewLists = new List<ViewList>();

            BindingContext = _viewModel = new TranslateViewModel()
            {
                WordId = wordId
            };
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            var setting = await App.SettingService.GetAsync();
            if (setting.PlaySoundInTranslatePageAutomatically)
            {
                if (setting.PlaySoundInTranslatePageAutomaticallyType == SoundType.Us)
                {
                    _viewModel.SoundUsImage = "VolumeBlueIcon";
                    _viewModel.SoundUsLabel = Color.FromHex("#105EFB");
                    _viewModel.SoundUkImage = "VolumeIcon";
                    _viewModel.SoundUkLabel = Color.FromHex("#707070");
                }
                if (setting.PlaySoundInTranslatePageAutomaticallyType == SoundType.Uk)
                {
                    _viewModel.SoundUkImage = "VolumeBlueIcon";
                    _viewModel.SoundUkLabel = Color.FromHex("#105EFB");
                    _viewModel.SoundUsImage = "VolumeIcon";
                    _viewModel.SoundUsLabel = Color.FromHex("#707070");
                }
            }

            _viewModel.WordEntity = await App.WordService.GetAsync(_viewModel.WordId);
            _viewModel.LessonEntity = await App.LessonService.GetAsync(_viewModel.WordEntity.LessonId);
            _viewModel.BookEntity = await App.BookService.GetAsync(_viewModel.LessonEntity.BookId);

            await ReadTranslate();
        }

        private async Task ReadTranslate()
        {
            _viewModel.SubTitle = "";
            _viewModel.Loading = true;
            Container.Children.Clear();

            _viewModel.SubTitle = _viewModel.WordEntity.Pronounce;

            var setting = await App.SettingService.GetAsync();
            if (setting.PlaySoundInTranslatePageAutomatically)
            {
                if (setting.PlaySoundInTranslatePageAutomaticallyType == SoundType.Us)
                {
                    await CrossTextToSpeech.Current.Speak(_viewModel.WordEntity.Title, App.LocaleUs, speakRate: 0.8f);
                }

                if (setting.PlaySoundInTranslatePageAutomaticallyType == SoundType.Uk)
                {
                    await CrossTextToSpeech.Current.Speak(_viewModel.WordEntity.Title, App.LocaleUk, speakRate: 0.8f);
                }
            }

            await Task.Delay(200);

            JArray obj = null;
            if (!string.IsNullOrWhiteSpace(_viewModel.WordEntity.GoogleData))
            {
                var bytes = Convert.FromBase64String(_viewModel.WordEntity.GoogleData);
                var json = Encoding.UTF8.GetString(bytes, 0, bytes.Length);
                obj = Newtonsoft.Json.JsonConvert.DeserializeObject<JArray>(json);
            }


            try
            {
                var v = FrameTranslations(obj);
                Container.Children.Add(v);
            }
            catch
            {
                //
            }
            try
            {
                var v = FrameDefinitions(obj);
                Container.Children.Add(v);
            }
            catch
            {
                //
            }
            try
            {
                var v = FrameExamples(obj);
                Container.Children.Add(v);
            }
            catch
            {
                //
            }
            try
            {
                var v = FrameSynonyms(obj);
                Container.Children.Add(v);
            }
            catch
            {
                //
            }

            _viewModel.Loading = false;
            _viewModel.IsLoaded = true;
        }

        private View FrameTranslations(JArray obj)
        {
            var list = new List<View>()
            {
                new Label()
                {
                    IsVisible = false,
                    Text = _viewModel.WordEntity.Translate,
                    FontSize = 20,
                    Margin = new Thickness(0,10,0,0),
                    FontAttributes = FontAttributes.Bold,
                }
            };

            if (obj != null)
                foreach (var groups in obj[1])
                {
                    list.Add(new Label()
                    {
                        IsVisible = false,
                        Margin = new Thickness(0, 10, 0, 5),
                        FontSize = 20,
                        Text = groups[0].Value<string>().First().ToString().ToUpper() +
                               groups[0].Value<string>().Substring(1),
                    });
                    foreach (var group in groups[2])
                    {
                        list.Add(new StackLayout()
                        {
                            IsVisible = false,
                            Margin = new Thickness(10, 10, 0, 0),
                            Children =
                        {
                            new Label()
                            {
                                FontSize = 18,
                                FontAttributes = FontAttributes.Bold,
                                Text = group[0].Value<string>(),
                                HorizontalTextAlignment = TextAlignment.Start,
                                HorizontalOptions = LayoutOptions.FillAndExpand
                            },
                            new Label()
                            {
                                TextColor = Color.Black,
                                FontSize = 18,
                                Text = string.Join(", ", group[1].Select(x => x.Value<string>()).ToList())
                            },
                        }
                        });
                    }
                }

            var stackLayout = new StackLayout()
            {
                Children =
                {
                    new StackLayout()
                    {
                        Orientation = StackOrientation.Horizontal,
                        Margin = new Thickness(0, 0, 0, 0),
                        Children =
                        {
                            new Image()
                            {
                                Source = "ArrowDownIcon",
                                Margin = new Thickness(0, 5, 10, 0),
                                WidthRequest = 20,
                                Aspect = Aspect.AspectFit,
                            },
                            new Label()
                            {
                                Text = "Translations",
                                FontSize = 20,
                                FontAttributes = FontAttributes.Bold,
                            },
                        },
                        GestureRecognizers =
                        {
                            new TapGestureRecognizer()
                            {
                                Command = new Command(() => HideShowElement("Translations"))
                            }
                        }
                    }
                },
            };

            foreach (var view in list)
            {
                _viewLists.Add(new ViewList()
                {
                    Id = "Translations",
                    View = view,
                    Toggle = "Hide"
                });
                stackLayout.Children.Add(view);
            }

            return new Frame()
            {
                BackgroundColor = Color.White,
                Margin = new Thickness(20, 10, 20, 10),
                CornerRadius = 5,
                Content = stackLayout
            };
        }
        private View FrameExamples(JArray obj)
        {
            var list = new List<View>();

            foreach (var group in obj[13][0])
            {
                list.Add(new StackLayout()
                {
                    IsVisible = false,
                    Orientation = StackOrientation.Horizontal,
                    Margin = new Thickness(0, 10, 0, 0),
                    Children =
                    {
                        new Image()
                        {
                            Source = "QuoteIcon",
                            VerticalOptions = LayoutOptions.Start,
                            Margin = new Thickness(0,10,15,0),
                            WidthRequest = 20,
                            Aspect = Aspect.AspectFit,
                        },
                        new Label()
                        {
                            TextColor = Color.Black,
                            FontSize = 18,
                            Text = StripHtml(group[0].Value<string>()),
                        },
                    },
                    GestureRecognizers =
                    {
                        new TapGestureRecognizer()
                        {
                            NumberOfTapsRequired = 1,
                            Command = new Command(async () => await OnExampleTab(group))
                        }
                    }
                });
            }

            var stackLayout = new StackLayout()
            {
                Children =
                {
                    new StackLayout()
                    {
                        Orientation = StackOrientation.Horizontal,
                        Margin = new Thickness(0, 0, 0, 0),
                        Children =
                        {
                            new Image()
                            {
                                Source = "ArrowDownIcon",
                                Margin = new Thickness(0, 5, 10, 0),
                                WidthRequest = 20,
                                Aspect = Aspect.AspectFit,
                            },
                            new Label()
                            {
                                Text = "Examples",
                                FontSize = 20,
                                FontAttributes = FontAttributes.Bold,
                            }
                        },
                        GestureRecognizers =
                        {
                            new TapGestureRecognizer()
                            {
                                Command = new Command(() => HideShowElement("Examples"))
                            }
                        }
                    }
                },
            };

            foreach (var view in list)
            {
                _viewLists.Add(new ViewList()
                {
                    Id = "Examples",
                    View = view,
                    Toggle = "Hide"
                });
                stackLayout.Children.Add(view);
            }

            return new Frame()
            {
                BackgroundColor = Color.White,
                Margin = new Thickness(20, 10, 20, 10),
                CornerRadius = 5,
                Content = stackLayout
            };
        }
        private async Task OnExampleTab(JToken group)
        {
            var text = StripHtml(group[0].Value<string>());
            var action = await DisplayActionSheet(null, null, null,
                "Listen Uk", "Listen Us", "Copy to Clipboard", "Share", "Open in google translate");
            if (action == "Listen Uk")
            {
                await CrossTextToSpeech.Current.Speak(text, App.LocaleUk, speakRate: 0.75f);
            }
            if (action == "Listen Us")
            {
                await CrossTextToSpeech.Current.Speak(text, App.LocaleUs, speakRate: 0.75f);
            }
            if (action == "Copy to Clipboard")
            {
                await Clipboard.SetTextAsync(text);
            }
            if (action == "Share")
            {
                await Share.RequestAsync(new ShareTextRequest
                {
                    Text = text,
                    Title = "Share Sentence"
                });
            }
            if (action == "Open in google translate")
            {
                await Browser.OpenAsync(
                    "https://translate.google.com/#view=home&op=translate&sl=en&tl=fa&text=" + text,
                    BrowserLaunchMode.SystemPreferred);
                return;
            }
        }
        private View FrameDefinitions(JArray obj)
        {
            var list = new List<View>();

            foreach (var groups in obj[12])
            {
                list.Add(new Label()
                {
                    Margin = new Thickness(0, 10, 0, 5),
                    FontSize = 20,
                    Text = groups[0].Value<string>().First().ToString().ToUpper() +
                           groups[0].Value<string>().Substring(1),
                });
                var n = 0;
                foreach (var group in groups[1])
                {
                    ++n;
                    //var isvisible = (n == 1);
                    var view = new StackLayout()
                    {
                        //IsVisible = isvisible,
                        Margin = new Thickness(0, 10, 0, 0),
                        Orientation = StackOrientation.Horizontal,
                        Children =
                        {
                            new Frame()
                            {
                                CornerRadius = 200,
                                BorderColor = Color.FromHex("#707070"),
                                Padding = new Thickness(15, 10, 15, 10),
                                VerticalOptions = LayoutOptions.Start,
                                HorizontalOptions = LayoutOptions.Center,
                                Content = new Label()
                                {
                                    FontSize = 14,
                                    FontAttributes = FontAttributes.Bold,
                                    Text = n.ToString(),
                                }
                            },
                            new StackLayout()
                            {
                                Margin = new Thickness(10, 0, 0, 0),
                                Children =
                                {
                                    new Label()
                                    {
                                        FontSize = 18,
                                        TextColor = Color.Black,
                                        Text = group[0].Value<string>(),
                                    },
                                    new Label()
                                    {
                                        FontSize = 18,
                                        Text = group.Count() > 2 ? group[2].Value<string>() : "",
                                    }
                                }
                            }
                        }
                    };
                    list.Add(view);
                    if (n > 1)
                    {
                        _viewLists.Add(new ViewList()
                        {
                            Id = "DefinitionsMore",
                            Toggle = "Hide",
                            View = view
                        });
                    }
                }
            }

            var stackLayout = new StackLayout()
            {
                Children =
                {
                    new StackLayout()
                    {
                        Orientation = StackOrientation.Horizontal,
                        Margin = new Thickness(0, 0, 0, 0),
                        Children =
                        {
                            new Image()
                            {
                                Source = "ArrowDownIcon",
                                Margin = new Thickness(0, 5, 10, 0),
                                WidthRequest = 20,
                                Aspect = Aspect.AspectFit,
                            },
                            new Label()
                            {
                                Text = "Definitions",
                                FontSize = 20,
                                FontAttributes = FontAttributes.Bold,
                            }
                        },
                        GestureRecognizers =
                        {
                            new TapGestureRecognizer()
                            {
                                Command = new Command(() => HideShowElement("Definitions"))
                            }
                        }
                    }
                },
            };

            foreach (var view in list)
            {
                _viewLists.Add(new ViewList()
                {
                    Id = "Definitions",
                    View = view,
                });
                stackLayout.Children.Add(view);
            }

            //var more = new StackLayout()
            //{
            //    GestureRecognizers =
            //    {
            //        new TapGestureRecognizer()
            //        {
            //            Command = new Command(() => HideShowElement("DefinitionsMore"))
            //        }
            //    },
            //    Children =
            //    {
            //        new StackLayout()
            //        {
            //            HeightRequest = 2,
            //            BackgroundColor = Color.FromHex("#ECECEC"),
            //            Margin = new Thickness(0, 10, 0, 10),
            //            VerticalOptions = LayoutOptions.Start,
            //            HorizontalOptions = LayoutOptions.FillAndExpand,
            //        },
            //        new Label()
            //        {
            //            FontSize = 16,
            //            Text = "More",
            //            HorizontalOptions = LayoutOptions.CenterAndExpand,
            //        }
            //    }
            //};
            //stackLayout.Children.Add(more);
            //_viewLists.Add(new ViewList()
            //{
            //    Id = "Definitions",
            //    View = more,
            //});

            return new Frame()
            {
                BackgroundColor = Color.White,
                Margin = new Thickness(20, 10, 20, 10),
                CornerRadius = 5,
                Content = stackLayout
            };
        }
        private View FrameSynonyms(JArray obj)
        {
            var list = new List<View>();

            foreach (var groups in obj[11])
            {
                list.Add(new Label()
                {
                    IsVisible = false,
                    Margin = new Thickness(0, 10, 0, 5),
                    FontSize = 20,
                    Text = groups[0].Value<string>().First().ToString().ToUpper() +
                           groups[0].Value<string>().Substring(1),
                });
                foreach (var group in groups[1])
                {
                    foreach (var items in group)
                    {
                        var views = new List<View>();
                        foreach (var item in items)
                        {
                            var view = new Frame()
                            {
                                Padding = 10,
                                Margin = 5,
                                BorderColor = Color.FromHex("#178A6E"),
                                HasShadow = false,
                                HorizontalOptions = LayoutOptions.Start,
                                Content = new Label()
                                {
                                    Text = item.Value<string>(),
                                    FontSize = 16
                                }
                            };
                            views.Add(view);
                        }
                        var flexLayout = new FlexLayout()
                        {
                            IsVisible = false,
                            AlignContent = FlexAlignContent.Center,
                            Wrap = FlexWrap.Wrap,
                            Margin = 0,
                            Padding = 0,
                            AlignItems = FlexAlignItems.Center
                        };
                        foreach (var item in views)
                        {
                            flexLayout.Children.Add(item);
                        }
                        list.Add(flexLayout);
                    }
                    list.Add(new StackLayout()
                    {
                        IsVisible = false,
                        BackgroundColor = Color.FromHex("#ECECEC"),
                        HeightRequest = 1,
                        Margin = 10,
                    });
                }
            }

            var stackLayout = new StackLayout()
            {
                Children =
                {
                    new StackLayout()
                    {
                        Orientation = StackOrientation.Horizontal,
                        Margin = new Thickness(0, 0, 0, 0),
                        Children =
                        {
                            new Image()
                            {
                                Source = "ArrowDownIcon",
                                Margin = new Thickness(0, 5, 10, 0),
                                WidthRequest = 20,
                                Aspect = Aspect.AspectFit,
                            },
                            new Label()
                            {
                                Text = "Synonyms",
                                FontSize = 20,
                                FontAttributes = FontAttributes.Bold,
                            }
                        },
                        GestureRecognizers =
                        {
                            new TapGestureRecognizer()
                            {
                                Command = new Command(() => HideShowElement("Synonyms"))
                            }
                        }
                    }
                },
            };

            foreach (var view in list)
            {
                _viewLists.Add(new ViewList()
                {
                    Id = "Synonyms",
                    View = view,
                    Toggle = "Hide"
                });
                stackLayout.Children.Add(view);
            }

            return new Frame()
            {
                BackgroundColor = Color.White,
                Margin = new Thickness(20, 10, 20, 10),
                CornerRadius = 5,
                Content = stackLayout
            };
        }

        private void HideShowElement(string id)
        {
            foreach (var viewList in _viewLists.Where(x => x.Id == id).ToList())
            {
                if (string.IsNullOrWhiteSpace(viewList.Toggle) || viewList.Toggle == "Show")
                {
                    viewList.View.IsVisible = false;
                    viewList.Toggle = "Hide";
                }
                else
                {
                    viewList.View.IsVisible = true;
                    viewList.Toggle = "Show";
                }
            }
        }

        private async void MenuClick(object sender, EventArgs e)
        {
            var list = new List<string>();

            if (_viewModel.BookEntity.Code.IsGuid())
            {
                list.Add("Edit word");
            }

            list.Add("Open in google translate");
            list.Add("Play audio");

            var action = await DisplayActionSheet(null, null, null,
                list.ToArray());
            if (action == "Open in google translate")
            {
                await Browser.OpenAsync(
                    "https://translate.google.com/#view=home&op=translate&sl=en&tl=fa&text=" +
                    _viewModel.WordEntity.Title,
                    BrowserLaunchMode.SystemPreferred);
                return;
            }
            if (action == "Play audio")
            {
                await Navigation.PushAsync(new PlaySoundPage(_viewModel.LessonEntity.Id));
            }
            if (action == "Edit word")
            {
                await Navigation.PushAsync(new SaveWordPage(_viewModel.BookEntity.Id, _viewModel.LessonEntity.Id, _viewModel.WordEntity.Id));
            }
        }

        private async void GoToPagePrev(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }

        private async void PlayUkSound(object sender, EventArgs e)
        {
            await CrossTextToSpeech.Current.Speak(_viewModel.WordEntity.Title, App.LocaleUk, speakRate: 0.8f);
        }
        private async void PlayUsSound(object sender, EventArgs e)
        {
            await CrossTextToSpeech.Current.Speak(_viewModel.WordEntity.Title, App.LocaleUs, speakRate: 0.8f);
        }
        private async void NextWord(object sender, EventArgs e)
        {
            var allwords = await App.WordService.SearchAsync(lessonId: _viewModel.WordEntity.LessonId);
            var arrays = allwords.ToArray();
            var index = Array.FindIndex(arrays, x => x.Id == _viewModel.WordEntity.Id);
            if ((arrays.Length - 1) < (index + 1))
            {
                return;
            }
            _viewModel.WordEntity = arrays[index + 1];
            _viewModel.WordId = _viewModel.WordEntity.Id;
            await ReadTranslate();
        }
        private async void PrevWord(object sender, EventArgs e)
        {
            var allwords = await App.WordService.SearchAsync(lessonId: _viewModel.WordEntity.LessonId);
            var arrays = allwords.ToArray();
            var index = Array.FindIndex(arrays, x => x.Id == _viewModel.WordEntity.Id);
            if (0 > (index - 1))
            {
                return;
            }
            _viewModel.WordEntity = arrays[index - 1];
            _viewModel.WordId = _viewModel.WordEntity.Id;
            await ReadTranslate();
        }

        private static string StripHtml(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }
    }
}