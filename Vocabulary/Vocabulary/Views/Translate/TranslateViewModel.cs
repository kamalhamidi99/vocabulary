﻿using Vocabulary.Entity;
using Vocabulary.ViewModels;
using Xamarin.Forms;

namespace Vocabulary.Views.Translate
{
    public class TranslateViewModel : BaseViewModel
    {
        public Command SoundUsCommand { get; set; }
       
        public TranslateViewModel()
        {
            SoundUsCommand = new Command(async () =>
            {
                var setting = await App.SettingService.GetAsync();

                setting.PlaySoundInTranslatePageAutomatically = !setting.PlaySoundInTranslatePageAutomatically;
                setting.PlaySoundInTranslatePageAutomaticallyType = SoundType.Us;

                if (setting.PlaySoundInTranslatePageAutomatically)
                {
                    SoundUsImage = "VolumeBlueIcon";
                    SoundUsLabel = Color.FromHex("#105EFB");
                    SoundUkImage = "VolumeIcon";
                    SoundUkLabel = Color.FromHex("#707070");
                }
                else
                {
                    SoundUsImage = "VolumeIcon";
                    SoundUsLabel = Color.FromHex("#707070");
                    SoundUkImage = "VolumeIcon";
                    SoundUkLabel = Color.FromHex("#707070");
                }

                await App.SettingService.UpdateAsync(setting);
            });
        }

        private string _soundUsImage = "VolumeIcon";
        public string SoundUsImage
        {
            get => _soundUsImage;
            set => SetProperty(ref _soundUsImage, value);
        }

        private Color _soundUsLabel = Color.FromHex("#707070");
        public Color SoundUsLabel
        {
            get => _soundUsLabel;
            set => SetProperty(ref _soundUsLabel, value);
        }

        private string _soundUkImage = "VolumeIcon";
        public string SoundUkImage
        {
            get => _soundUkImage;
            set => SetProperty(ref _soundUkImage, value);
        }

        private Color _soundUkLabel = Color.FromHex("#707070");
        public Color SoundUkLabel
        {
            get => _soundUkLabel;
            set => SetProperty(ref _soundUkLabel, value);
        }

        public bool IsLoaded { get; set; }

        public int BookId { get; set; }
        private BookEntity _bookEntity = new BookEntity();
        public BookEntity BookEntity
        {
            get => _bookEntity;
            set => SetProperty(ref _bookEntity, value);
        }

        public int LessonId { get; set; }
        private LessonEntity _lessonEntity = new LessonEntity();
        public LessonEntity LessonEntity
        {
            get => _lessonEntity;
            set => SetProperty(ref _lessonEntity, value);
        }

        public int WordId { get; set; }
        private WordEntity _wordEntity = new WordEntity();
        public WordEntity WordEntity
        {
            get => _wordEntity;
            set => SetProperty(ref _wordEntity, value);
        }

        private string _subtitle;
        public string SubTitle
        {
            get => _subtitle;
            set => SetProperty(ref _subtitle, value);
        }

    }
}