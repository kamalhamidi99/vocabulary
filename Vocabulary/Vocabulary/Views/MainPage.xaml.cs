﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using Xamarin.Forms;

using Vocabulary.Models;
using Vocabulary.Views.Book;
using Vocabulary.Views.Collection.AddBook;
using Vocabulary.Views.DownloadBook;
using Vocabulary.Views.Search;
using Vocabulary.Views.Setting;

namespace Vocabulary.Views
{
    [DesignTimeVisible(false)]
    public partial class MainPage
    {
        private readonly Dictionary<int, NavigationPage> _menuPages = new Dictionary<int, NavigationPage>();
        public MainPage()
        {
            InitializeComponent();

            MasterBehavior = MasterBehavior.Popover;

            _menuPages.Add((int)MenuItemType.BookPage, (NavigationPage)Detail);
        }

        public async Task NavigateFromMenu(int id)
        {
            if (!_menuPages.ContainsKey(id))
            {
                switch (id)
                {
                    case (int)MenuItemType.BookPage:
                        _menuPages.Add(id, new NavigationPage(new BookPage()));
                        break;
                    case (int)MenuItemType.DownloadBook:
                        _menuPages.Add(id, new NavigationPage(new DownloadBookPage()));
                        break;
                    case (int)MenuItemType.SearchPage:
                        _menuPages.Add(id, new NavigationPage(new SearchPage()));
                        break;
                    case (int)MenuItemType.AddBook:
                        _menuPages.Add(id, new NavigationPage(new AddBookPage()));
                        break;
                    case (int)MenuItemType.Setting:
                        _menuPages.Add(id, new NavigationPage(new SettingPage()));
                        break;
                }
            }

            var newPage = _menuPages[id];

            if (newPage != null && Detail != newPage)
            {
                Detail = newPage;

                if (Device.RuntimePlatform == Device.Android)
                    await Task.Delay(100);

                IsPresented = false;
            }
        }
    }
}