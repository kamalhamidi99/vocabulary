﻿using Vocabulary.ViewModels;

namespace Vocabulary.Views.Book
{
    public class BookViewModel : BaseViewModel
    {
        int _count;
        public int Count
        {
            get => _count;
            set => SetProperty(ref _count, value);
        }
    }
}