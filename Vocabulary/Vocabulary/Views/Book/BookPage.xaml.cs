﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using RestSharp;
using Vocabulary.Entity;
using Vocabulary.Models;
using Vocabulary.Renderers;
using Vocabulary.Views.Lesson;
using Vocabulary.Views.Word;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Vocabulary.Views.Book
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BookPage
    {
        private readonly BookViewModel _viewModel;
        private static MainPage RootPage => Application.Current.MainPage as MainPage;
        private readonly IList<ViewList> _viewLists;

        public BookPage()
        {
            InitializeComponent();

            _viewLists = new List<ViewList>();

            BindingContext = _viewModel = new BookViewModel();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            await LoadOnAppearing();
        }

        private async Task LoadOnAppearing()
        {
            _viewModel.Loading = true;
            Container.Children.Clear();
            NothingToShowHere.IsVisible = false;
            await Task.Delay(100);

            try
            {
                //var view = new Button()
                //{
                //    BackgroundColor = Color.Red,
                //    Text = "click",
                //};
                //view.Effects.Add(new LongPressedEffect());
                //LongPressedEffect.SetCommand(view, new Command(() => { DisplayAlert("", "long press", "ok"); }));
                //view.Clicked += (sender, args) => { DisplayAlert("", "click", "ok"); };
                //Container.Children.Add(view);

                var getall = await App.BookService.GetAllAsync() ?? new List<BookEntity>();
                _viewModel.Count = getall.Count;
                if (_viewModel.Count == 0)
                {
                    NothingToShowHere.IsVisible = true;
                    _viewModel.Loading = false;
                    return;
                }

                foreach (var item in getall)
                {
                    item.CoverImage = ImageSource.FromStream(
                        () => new MemoryStream(Convert.FromBase64String(item.Cover)));

                    var id = Guid.NewGuid().ToString().Replace("-", "");
                    var frame = new Frame()
                    {
                        Margin = 0,
                        Padding = 5,
                        BackgroundColor = Color.White,
                        BorderColor = Color.FromHex("#707070"),
                        CornerRadius = 5,
                        VerticalOptions = LayoutOptions.Start,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        HasShadow = false,
                        Content = new Grid()
                        {
                            Children =
                            {
                                new StackLayout()
                                {
                                    Orientation = StackOrientation.Horizontal,
                                    Children =
                                    {
                                        new Frame()
                                        {
                                            CornerRadius = 5,
                                            Padding = 0,
                                            VerticalOptions = LayoutOptions.Start,
                                            IsClippedToBounds = true,
                                            HasShadow = false,
                                            Content = new Image()
                                            {
                                                WidthRequest = 60,
                                                HeightRequest = 80,
                                                Aspect = Aspect.Fill,
                                                HorizontalOptions = LayoutOptions.Center,
                                                VerticalOptions = LayoutOptions.Center,
                                                Source = item.CoverImage
                                            }
                                        },
                                        new StackLayout()
                                        {
                                            HorizontalOptions = LayoutOptions.FillAndExpand,
                                            VerticalOptions = LayoutOptions.CenterAndExpand,
                                            Children =
                                            {
                                                new Label()
                                                {
                                                    Text = item.Title,
                                                    TextColor = Color.FromHex("#707070"),
                                                    HorizontalOptions = LayoutOptions.StartAndExpand,
                                                    LineBreakMode = LineBreakMode.WordWrap,
                                                    FontSize = 20,
                                                    FontAttributes = FontAttributes.Bold,
                                                    HorizontalTextAlignment = TextAlignment.Start,
                                                },
                                                new Label()
                                                {
                                                    Text = item.Level,
                                                    TextColor = Color.FromHex("#707070"),
                                                    HorizontalOptions = LayoutOptions.StartAndExpand,
                                                    LineBreakMode = LineBreakMode.WordWrap,
                                                    Margin = new Thickness(0, -8, 0, 0),
                                                    HorizontalTextAlignment = TextAlignment.Start,
                                                },
                                                new Label()
                                                {
                                                    Text = item.Writer,
                                                    TextColor = Color.FromHex("#707070"),
                                                    HorizontalOptions = LayoutOptions.StartAndExpand,
                                                    LineBreakMode = LineBreakMode.WordWrap,
                                                    Margin = new Thickness(0, -8, 0, 0),
                                                    HorizontalTextAlignment = TextAlignment.Start,
                                                },
                                            }
                                        }
                                    }
                                },
                                new StackLayout()
                                {
                                    BackgroundColor = Color.Transparent,
                                    GestureRecognizers =
                                    {
                                        new TapGestureRecognizer()
                                        {
                                            Command = new Command(async () =>
                                                await Navigation.PushAsync(new LessonPage(item.Id)))
                                        },
                                        new TapGestureRecognizer()
                                        {
                                            NumberOfTapsRequired = 2,
                                            Command = new Command(() => { })
                                        },
                                        new SwipeGestureRecognizer()
                                        {
                                            Direction = SwipeDirection.Right,
                                            Command = new Command(() => ToRightAndHide(id))
                                        },
                                        new SwipeGestureRecognizer()
                                        {
                                            Direction = SwipeDirection.Left,
                                            Command = new Command(() => ToLeftAndShow(id))
                                        },
                                    }
                                }
                            }
                        },
                    };
                    _viewLists.Add(new ViewList()
                    {
                        View = frame,
                        Id = id + "-frame",
                    });
                    var image = new Image()
                    {
                        Source = "DeleteIcon",
                        WidthRequest = 30,
                        HorizontalOptions = LayoutOptions.End,
                        Margin = new Thickness(10, 10, 15, 10),
                        Aspect = Aspect.AspectFit,
                        IsVisible = false,
                        GestureRecognizers =
                        {
                            new TapGestureRecognizer()
                            {
                                Command = new Command(async () => await DeleteBook(item.Id))
                            }
                        }
                    };
                    _viewLists.Add(new ViewList()
                    {
                        View = image,
                        Id = id + "-image",
                    });
                    Container.Children.Add(new Frame()
                    {
                        Margin = new Thickness(20, 5, 20, 5),
                        Padding = 0,
                        BackgroundColor = Color.FromHex("#10775E"),
                        BorderColor = Color.FromHex("#707070"),
                        CornerRadius = 5,
                        VerticalOptions = LayoutOptions.Start,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        HasShadow = false,
                        Content = new Grid()
                        {
                            Children =
                            {
                                frame,
                                image
                            }
                        }
                    });
                }

                _viewModel.Loading = false;
            }
            catch (Exception ex)
            {
                _viewModel.Loading = false;
                await DisplayAlert("Error", ex.Message, "OK");
            }
            finally
            {
                _viewModel.Loading = false;
            }
        }

        private async Task DeleteBook(int id)
        {
            var action = await DisplayAlert("Confirm", "Are you sure?", "yes", "no");
            if (action)
            {
                _viewModel.LoadingWait = true;
                var result = await App.BookService.DeleteAsync(id);
                if (!result)
                {
                    await DisplayAlert("Error", "Can not delete this book.", "OK");
                    return;
                }
                _viewModel.LoadingWait = false;
                await LoadOnAppearing();
            }
        }

        public void ToRightAndHide(string id)
        {
            var image = _viewLists.First(x => x.Id == id + "-image");
            image.View.IsVisible = false;
            image.Toggle = true.ToString();
            var frame = _viewLists.First(x => x.Id == id + "-frame");
            frame.View.TranslateTo(0, 0, 100);
            frame.Toggle = true.ToString();
        }
        public void ToLeftAndShow(string id)
        {
            var frame = _viewLists.First(x => x.Id == id + "-frame");
            frame.View.TranslateTo(-50, 0, 100);
            frame.Toggle = true.ToString();
            var image = _viewLists.First(x => x.Id == id + "-image");
            var imageview = (Image)image.View;
            imageview.IsVisible = true;
            imageview.Source = "DeleteIcon";
            image.Toggle = true.ToString();
        }

        private void OpenMenu(object sender, EventArgs e)
        {
            RootPage.IsPresented = true;
        }

    }
}