﻿using System.Collections.Generic;
using SQLite;

namespace Vocabulary.Entity
{
    public class LessonEntity
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [Indexed]
        public int BookId { get; set; }

        public string Title { get; set; }

        public int Number { get; set; }

        [Ignore]
        public IList<WordEntity> Words { get; set; }
    }
}