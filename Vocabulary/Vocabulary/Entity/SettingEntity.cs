﻿using SQLite;

namespace Vocabulary.Entity
{
    public class SettingEntity
    {
        public SettingEntity()
        {
            Id = 1;
        }

        [PrimaryKey]
        public int Id { get; set; }

        public bool PlaySoundInTranslatePageAutomatically { get; set; }
        public SoundType? PlaySoundInTranslatePageAutomaticallyType { get; set; }
    }

    public enum SoundType
    {
        Uk,
        Us
    }
}
