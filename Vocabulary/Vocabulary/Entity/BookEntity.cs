﻿using System.Collections.Generic;
using SQLite;
using Xamarin.Forms;

namespace Vocabulary.Entity
{
    public class BookEntity
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public string Code { get; set; }

        public string Level { get; set; }

        public string Title { get; set; }

        public string Writer { get; set; }

        public string Cover { get; set; }

        public string Link { get; set; }

        public int Version { get; set; }

        [Ignore]
        public ImageSource CoverImage { get; set; }
        [Ignore]
        public string IconSource { get; set; }
        [Ignore]
        public string IconLabel { get; set; }
        [Ignore]
        public IList<LessonEntity> Lessons { get; set; }
    }
}
