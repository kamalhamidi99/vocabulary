﻿using SQLite;

namespace Vocabulary.Entity
{
    public class WordEntity
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [Indexed]
        public int BookId { get; set; }

        [Indexed]
        public int LessonId { get; set; }

        public string Title { get; set; }

        public string Pronounce { get; set; }

        public string Translate { get; set; }

        public string GoogleData { get; set; }
    }
}