﻿using System;
using System.IO;
using System.Linq;
using Plugin.TextToSpeech;
using Plugin.TextToSpeech.Abstractions;
using SQLite;
using Vocabulary.Services;
using Vocabulary.Views;

namespace Vocabulary
{
    public partial class App 
    {
        public App()
        {
            InitializeComponent();
            //MainPage = new NavigationPage(new PlaySoundPage(641));
            MainPage = new MainPage();
        }

        private static SQLiteAsyncConnection _sqLiteAsyncConnection;
        public static SQLiteAsyncConnection SqLiteAsyncConnection => _sqLiteAsyncConnection ?? (_sqLiteAsyncConnection =
                                                                         new SQLiteAsyncConnection(
                                                                             Path.Combine(
                                                                                 Environment.GetFolderPath(Environment
                                                                                     .SpecialFolder
                                                                                     .LocalApplicationData),
                                                                                 "Vocabulary.db3")));

        static BookService _bookService;
        public static BookService BookService => _bookService ?? (_bookService = new BookService());

        static LessonService _lessonService;
        public static LessonService LessonService => _lessonService ?? (_lessonService = new LessonService());

        static WordService _wordService;
        public static WordService WordService => _wordService ?? (_wordService = new WordService());

        static SettingService _settingService;
        public static SettingService SettingService => _settingService ?? (_settingService = new SettingService());

        public static CrossLocale LocaleUs;
        public static CrossLocale LocaleUk;

        protected override async void OnStart()
        {
            var locales = (await CrossTextToSpeech.Current.GetInstalledLanguages()).ToList();
            var enumerable = locales.ToList();
            LocaleUk = enumerable.FirstOrDefault(x => x.Country == "GB" && x.Language == "en");
            LocaleUs = enumerable.FirstOrDefault(x => x.Country == "US" && x.Language == "en");
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
