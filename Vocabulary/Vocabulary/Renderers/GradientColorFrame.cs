﻿using Xamarin.Forms;

namespace Vocabulary.Renderers
{
    public class GradientColorFrame : Frame
    {
        public Color StartColor { get; set; }
        public Color EndColor { get; set; }
    }
}