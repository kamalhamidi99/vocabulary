﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Vocabulary.Extension
{
    public static class HttpClientExtension
    {
        public static async Task<HttpClientResult> SendAndReadAsByteAsync(HttpMethod httpMethod, string url, IDictionary<string, string> headers = null)
        {
            var result = new HttpClientResult();

            using (var client = new HttpClient() {MaxResponseContentBufferSize = 256000})
            {
                try
                {
                    var uri = new Uri(url);

                    var httpRequestMessage = new HttpRequestMessage(httpMethod, uri);

                    if (headers == null || !headers.Any())
                    {
                        headers = new Dictionary<string, string>();
                    }

                    foreach (var header in headers)
                    {
                        httpRequestMessage.Headers.Add(header.Key, header.Value);
                    }

                    var response = await client.SendAsync(httpRequestMessage);

                    result.StatusCode = response.StatusCode;
                    result.ResponseAsByte =  await response.Content.ReadAsByteArrayAsync();
                    return result;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(@"				ERROR {0}", ex.Message);
                }
            }

            result.StatusCode = HttpStatusCode.ExpectationFailed;
            return result;
        }
        public static async Task<HttpClientResult> SendAsync(HttpMethod httpMethod, string url, IDictionary<string, string> headers = null)
        {
            var result = new HttpClientResult();

            using (var client = new HttpClient() {MaxResponseContentBufferSize = int.MaxValue})
            {
                try
                {
                    var uri = new Uri(url);

                    var httpRequestMessage = new HttpRequestMessage(httpMethod, uri);

                    if (headers == null || !headers.Any())
                    {
                        headers = new Dictionary<string, string>();
                    }

                    foreach (var header in headers)
                    {
                        httpRequestMessage.Headers.Add(header.Key, header.Value);
                    }

                    var response = await client.SendAsync(httpRequestMessage);

                    result.StatusCode = response.StatusCode;
                    result.HttpResponseMessage = response;
                    return result;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(@"				ERROR {0}", ex.Message);
                }
            }

            result.StatusCode = HttpStatusCode.ExpectationFailed;
            return result;
        }

        public class HttpClientResult
        {
            public HttpStatusCode StatusCode { get; set; }

            public HttpResponseMessage HttpResponseMessage { get; set; }

            public string ResponseMessage { get; set; }
            public byte[] ResponseAsByte { get; set; }
        }
    }
}