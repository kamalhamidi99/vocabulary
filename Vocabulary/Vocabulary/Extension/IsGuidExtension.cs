﻿using System;

namespace Vocabulary.Extension
{
    public static class IsGuidExtension
    {
        public static bool IsGuid(this string data)
        {
            return Guid.TryParse(data, out _);
        }
    }
}
