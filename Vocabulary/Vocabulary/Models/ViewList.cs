﻿using Xamarin.Forms;

namespace Vocabulary.Models
{
    public class ViewList
    {
        public string Id { get; set; }
        public View View { get; set; }
        public string Toggle { get; set; }
    }
}