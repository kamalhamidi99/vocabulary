﻿namespace Vocabulary.Models
{
    public class CheckForUpdate
    {
        public string Version { get; set; }

        public string Link { get; set; }
    }
}