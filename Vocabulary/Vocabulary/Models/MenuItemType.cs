﻿
namespace Vocabulary.Models
{
    public enum MenuItemType
    {
        DownloadBook,
        BookPage,
        SearchPage,
        AddBook,
        Setting,
    }
}
