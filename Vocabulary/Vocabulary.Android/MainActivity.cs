﻿using Android.App;
using Android.Content.PM;
using Android.Net;
using Android.Runtime;
using Android.OS;
using Android.Views;
using Vocabulary.Droid.Bugs;

namespace Vocabulary.Droid
{
    [Activity(Label = "Vocabulary", Icon = "@mipmap/ic_launcher", Theme = "@style/MainTheme", MainLauncher = false, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        public static ConnectivityManager ConnectivityManager { get; set; }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            ConnectivityManager = (ConnectivityManager)GetSystemService(ConnectivityService);

            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);

            // Fix the keyboard so it doesn't overlap the grid icons above keyboard etc, and makes Android 5+ work as AdjustResize in Android 4
            Window.SetSoftInputMode(SoftInput.AdjustResize);
            if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
            {
                // Bug in Android 5+, this is an adequate workaround
                AndroidBug5497WorkaroundForXamarinAndroid.assistActivity(this);
            }

            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            LoadApplication(new App());
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}