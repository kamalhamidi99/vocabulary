﻿using Android.App;
using Android.Content.PM;
using Android.OS;

namespace Vocabulary.Droid
{
    [Activity(Label = "Vocabulary", Icon = "@mipmap/ic_launcher", Theme = "@style/Start", MainLauncher = true, ConfigurationChanges = ConfigChanges.Orientation, NoHistory = true)]
    public class StartActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            System.Threading.Thread.Sleep(1);
            StartActivity(typeof(MainActivity));
        }
    }
}