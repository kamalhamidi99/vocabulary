﻿using Android.App;
using Android.Widget;
using Vocabulary.DependencyInterfaces;
using Vocabulary.Droid.DependencyServices;

[assembly: Xamarin.Forms.Dependency(typeof(MessageToast))]
namespace Vocabulary.Droid.DependencyServices
{
    public class MessageToast : IMessageToast
    {
        public void LongAlert(string message)
        {
            Toast.MakeText(Application.Context, message, ToastLength.Long).Show();
        }

        public void ShortAlert(string message)
        {
            Toast.MakeText(Application.Context, message, ToastLength.Short).Show();
        }
    }
}