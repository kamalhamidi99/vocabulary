﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Vocabulary.DependencyInterfaces;
using Vocabulary.Droid.DependencyServices;
using Xamarin.Forms;

[assembly: Dependency(typeof(PersianDateService))]
namespace Vocabulary.Droid.DependencyServices
{
    public class PersianDateService : IPersianDateService
    {
        public string GetPersianDate(DateTime dateTime, bool? withTime = true)
        {
            var pc = new PersianCalendar();

            return withTime != null && withTime == true
                ? $"{pc.GetYear(dateTime)}/{pc.GetMonth(dateTime)}/{pc.GetDayOfMonth(dateTime)} {pc.GetHour(dateTime)}:{pc.GetMinute(dateTime)}"
                : $"{pc.GetYear(dateTime)}/{pc.GetMonth(dateTime)}/{pc.GetDayOfMonth(dateTime)}";
        }

        public string GetPersianDate(bool? withTime = true)
        {
            var pc = new PersianCalendar();
            var dateTime = DateTime.Now;

            return withTime != null && withTime == true
                ? $"{pc.GetYear(dateTime)}/{pc.GetMonth(dateTime)}/{pc.GetDayOfMonth(dateTime)} {pc.GetHour(dateTime)}:{pc.GetMinute(dateTime)}"
                : $"{pc.GetYear(dateTime)}/{pc.GetMonth(dateTime)}/{pc.GetDayOfMonth(dateTime)}";
        }

        public IList<string> GetPersianDateArray(DateTime? dateTime = null)
        {
            var pc = new PersianCalendar();
            dateTime ??= DateTime.Now;

            return
                $"{pc.GetYear(dateTime.Value)}/{pc.GetMonth(dateTime.Value)}/{pc.GetDayOfMonth(dateTime.Value)}/{pc.GetHour(dateTime.Value)}/{pc.GetMinute(dateTime.Value)}"
                    .Split(Convert.ToChar("/"));
        }

        public IDictionary<int, string> GetPersianMonthName()
        {
            var divMonthInYear = new Dictionary<int, string>
            {
                {1,"فروردین"},{2,"اردیبهشت"},{3,"خرداد"},
                {4,"تیر"},{5,"مرداد"},{6,"شهریور"},
                {7,"مهر"},{8,"آبان"},{9,"آذر"},
                {10,"دی"},{11,"بهمن"},{12,"اسفند"}
            };

            return divMonthInYear;
        }
        public string GetPersianMonthName(int month)
        {
            var divMonthInYear = new Dictionary<int, string>
            {
                {1,"فروردین"},{2,"اردیبهشت"},{3,"خرداد"},
                {4,"تیر"},{5,"مرداد"},{6,"شهریور"},
                {7,"مهر"},{8,"آبان"},{9,"آذر"},
                {10,"دی"},{11,"بهمن"},{12,"اسفند"}
            };

            return divMonthInYear[month];
        }

    }
}